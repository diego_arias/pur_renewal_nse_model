

```python
import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import check_random_state
from sklearn.cross_validation import train_test_split
from sklearn.utils import resample

from sklearn.ensemble import GradientBoostingClassifier as gbc

from sklearn.metrics import (mean_squared_error,mean_absolute_error,
                             r2_score,explained_variance_score, roc_auc_score,
                            classification_report, confusion_matrix,
                            roc_curve, accuracy_score)

from sklearn.ensemble import partial_dependence

from analytics import *

import pickle

from dfply import *
from plotnine import *
import datetime as dt
from mizani.formatters import percent_format
from mizani.formatters import comma_format
import matplotlib.pyplot as plt
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/sklearn/cross_validation.py:41: DeprecationWarning: This module was deprecated in version 0.18 in favor of the model_selection module into which all the refactored classes and functions are moved. Also note that the interface of the new CV iterators are different from that of this module. This module will be removed in 0.20.
      "This module will be removed in 0.20.", DeprecationWarning)
    /Users/darias/anaconda3/lib/python3.7/site-packages/sklearn/ensemble/weight_boosting.py:29: DeprecationWarning: numpy.core.umath_tests is an internal NumPy module and should not be imported. It will be removed in a future NumPy release.
      from numpy.core.umath_tests import inner1d


# Introduction

We need to have the best _redeem model_, we have two past iterations of this model.

In this third iteration, we are going to add two variables to the PUR formula:
 - Extensions
 - Renewals

$$PUR_{customer} = \frac{\sum_i Redeem_{customer} + \sum_i Extensions_{customer} + \sum_i Renewal_{customer}}{\sum_i Redeem_{customer} + \sum_i Extensions_{customer} + \sum_i Renewal_{customer} + \sum_i Drop_{customer}}$$

Also included as features

We know the ZIP code of our customers, so, using this, we are going to join a ZIP-SocioeconomicLevel table with the data, to add the customer's socioeconomic level to the model.

In Mexico there are two types of socioeconomic level:
 - Urban
 - rural

And each type, different levels:
 - A/B
 - C+
 - C
 - C-
 - D
 - E

So, with the SEL (Socioeconomic Level) we can know two things of our customers:
 - If they live in a rural or urban area
 - The level of their incomes

# Data Explanation

The other features of the last model are going to be used also in this one.

# Querys

## LoanSummary & Customer features

From LoanSummary and CustomerTable from EzPawnInfo, were created features for each customer, with 2018 data:

 - Number of loans
 - Items featrures: average, standard deviation, etc
 - Original Loan features: average, standard deviation, etc
 - Extensions features: average, standard deviation, etc
 - Extension Dollars features: average, standard deviation, etc
 - Renewal & Renewal Dollars features
 - Birthday (to calculate the age)
 - Gender
 - ZIP code

    WITH LOANSUMMARY AS (
    SELECT
     *
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary
     WHERE
      lastAction IN ('Loan Redemption', 'Drop Loan') AND
      DATEPART(yyyy,OriginDate) = 2018               AND
      LoanCountry               = 'Mex'              
    ) aux
    WHERE
     rank > 1
    )
    
    
    SELECT
     LSF.*,
     cust.Gender,
     cust.City,
     cust.state,
     cust.birth_date,
     cust.zip
    FROM(
     SELECT
      ls.Customer                                         AS Customer,
      COUNT(1)                                            AS total_loans,
    
      SUM(CASE
           WHEN ls.lastAction = 'Loan Redemption'
           THEN 1 ELSE 0 END)                             AS total_loan_redemption,
      SUM(CASE
           WHEN ls.lastAction = 'Drop Loan'
           THEN 1 ELSE 0 END)                             AS total_drop_loan,
    
      SUM(CASE
           WHEN ls.lastAction = 'Loan Redemption'
           THEN ls.OriginalLoan ELSE 0 END)               AS total_Originalloan_loan_redemption,
      SUM(CASE
           WHEN ls.lastAction = 'Drop Loan'
           THEN ls.OriginalLoan ELSE 0 END)               AS total_Originalloan_drop_loan,

      SUM(CASE
           WHEN ls.lastAction = 'Loan Redemption'
           THEN ls.RenewalDollars ELSE 0 END)               AS total_RenewalDollars_loan_redemption,
      SUM(CASE
           WHEN ls.lastAction = 'Drop Loan'
           THEN ls.RenewalDollars ELSE 0 END)               AS total_RenewalDollars_drop_loan,
      
      SUM(ls.items)                                       AS total_items,
      AVG(CAST(ls.items AS FLOAT))                        AS avg_items_per_loan,
      STDEVP(ls.items)                                    AS sd_items_per_loan,
      MAX(ls.items)                                       AS max_items_per_loan, 
      
      SUM(ls.OriginalLoan)                                AS total_originalloan,
      AVG(ls.OriginalLoan)                                AS avg_originalloan_per_loan,
      STDEVP(ls.OriginalLoan)                             AS sd_originalloan_per_loan,
      
      SUM(ls.Extensions)                                  AS total_extensions,
      AVG(CAST(ls.Extensions AS FLOAT))                   AS avg_extensions_per_loan,
      STDEVP(ls.Extensions)                               AS sd_extensions_per_loan,
      MIN(ls.Extensions)                                  AS min_extensions_per_loan,
      MAX(ls.Extensions)                                  AS max_extensions_per_loan,
    
      SUM(ls.ExtensionDollars)                            AS total_extensiondollars,
      AVG(ls.ExtensionDollars)                            AS avg_extensiondollars_per_loan,
      STDEVP(ls.ExtensionDollars)                         AS sd_extensiondollars_per_loan,
    
      SUM(ls.Renewals)                                    AS total_renewals,
      AVG(ls.Renewals)                                    AS avg_renewals,
      MAX(ls.Renewals)                                    AS max_renewals,
      MIN(ls.Renewals)                                    AS min_renewals,
    
      SUM(ls.RenewalDollars)                              AS total_RenewalDollars,
      AVG(ls.RenewalDollars)                              AS avg_RenewalDollars,
      STDEVP(ls.RenewalDollars)                           AS sd_RenewalDollars,
       
          AVG(ls.ExtensionDollars/ls.OriginalLoan)            AS avg_pct_extensiondollars_originalloan_per_loan,
      AVG(ls.RenewalDollars/ls.OriginalLoan)              AS avg_pct_RenewalDollars_originalloan_per_loan
     FROM
      LOANSUMMARY ls
     GROUP BY
      ls.Customer) LSF
    LEFT JOIN
     dbo.CustomerTable cust
    ON
         cust.CustomerID=LSF.Customer

## Category features

From **CategoryTable**, from **EzPawnInfo**, were created features for each customer, with 2018 data:

  - Number of items loaned for each secondary category

    WITH LOANSUMMARY AS (
    SELECT
     *
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary
     WHERE
      lastAction IN ('Loan Redemption', 'Drop Loan') AND
      DATEPART(yyyy,OriginDate) = 2018               AND
      LoanCountry               = 'Mex'              
    ) aux
    WHERE
     rank > 1
    )


    SELECT
     aux.Customer,
     SUM(CASE WHEN cat.Secondary = 'Tools' THEN 1 ELSE 0 END)                  AS Tools,
     SUM(CASE WHEN cat.Secondary = 'Electronics' THEN 1 ELSE 0 END)            AS Electronics,
     SUM(CASE WHEN cat.Secondary = 'Household/Office Goods' THEN 1 ELSE 0 END) AS Household_Office_Goods,
     SUM(CASE WHEN cat.Secondary = 'Musical Instruments' THEN 1 ELSE 0 END)    AS Musical_Instruments,
     SUM(CASE WHEN cat.Secondary = 'Sporting Goods' THEN 1 ELSE 0 END)         AS Sporting_Goods,
     SUM(CASE WHEN cat.Secondary = 'Transportation' THEN 1 ELSE 0 END)         AS Transportation,
     SUM(CASE WHEN cat.Secondary = 'Bracelet' THEN 1 ELSE 0 END)               AS Bracelet,
     SUM(CASE WHEN cat.Secondary = 'Ring' THEN 1 ELSE 0 END)                   AS Ring,
     SUM(CASE WHEN cat.Secondary = 'Chain' THEN 1 ELSE 0 END)                  AS Chain,
     SUM(CASE WHEN cat.Secondary = 'Yard Equipment' THEN 1 ELSE 0 END)         AS Yard_Equipment,
     SUM(CASE WHEN cat.Secondary = 'Camera Equipment' THEN 1 ELSE 0 END)       AS Camera_Equipment,
     SUM(CASE WHEN cat.Secondary = 'Pendant/Locket' THEN 1 ELSE 0 END)         AS Pendant_Locket,
     SUM(CASE WHEN cat.Secondary = 'Gun & Police Equipment' THEN 1 ELSE 0 END) AS Gun_Police_Equipment,
     SUM(CASE WHEN cat.Secondary = 'Earrings' THEN 1 ELSE 0 END)               AS Earrings,
     SUM(CASE WHEN cat.Secondary = 'Hand Guns' THEN 1 ELSE 0 END)              AS Hand_Guns,
     SUM(CASE WHEN cat.Secondary = 'Long Guns' THEN 1 ELSE 0 END)              AS Long_Guns,
     SUM(CASE WHEN cat.Secondary = 'Money Clip' THEN 1 ELSE 0 END)             AS Money_Clip,
     SUM(CASE WHEN cat.Secondary = 'Watch' THEN 1 ELSE 0 END)                  AS Watch,
     SUM(CASE WHEN cat.Secondary = 'Other' THEN 1 ELSE 0 END)                  AS Other
    FROM(
     SELECT
      LS.Customer   AS Customer,
      it.CategoryID AS CategoryID
     FROM
      LOANSUMMARY LS
     LEFT JOIN
      dbo.ItemTable it
     ON
      it.LoanID=LS.PrimaryLoan) aux
    LEFT JOIN dbo.CategoryTable cat ON cat.CategoryID=aux.CategoryID
    GROUP BY
     aux.Customer

## LoanSummary days features

From **LoanSummary** afrom **EzPawnInfo**, were created features for each customer, with 2018 data:

  - Days between each loan: average, sum, etc

    WITH LOANSUMMARY AS (
    SELECT
     *
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary
     WHERE
      lastAction IN ('Loan Redemption', 'Drop Loan') AND
      DATEPART(yyyy,OriginDate) = 2018               AND
      LoanCountry               = 'Mex'              
    ) aux
    WHERE
     rank > 1
    )

    SELECT
     loan_lead.Customer                                            AS Customer,
     AVG(loan_lead.difference_days)                                AS avg_days_for_loan,
     STDEVP(loan_lead.difference_days)                             AS sd_days_for_loan,
     MAX(loan_lead.difference_days)                                AS max_days_for_loan,
     MIN(loan_lead.difference_days)                                AS min_days_for_loan,
     MAX(loan_lead.difference_days)-AVG(loan_lead.difference_days) AS dif_max_avg_days_for_loan,
     AVG(loan_lead.difference_days)-min(loan_lead.difference_days) AS dif_avg_min_days_for_loan
    FROM
    (
    SELECT
     Customer,
     DATEDIFF(day,OriginDate,LEAD(OriginDate) OVER (PARTITION BY Customer ORDER BY OriginDate ASC)) AS difference_days
    FROM
     LOANSUMMARY
    ) loan_lead
    GROUP BY
     loan_lead.Customer

## Y

From **LoanSummary** afrom **EzPawnInfo**, was created de **y** variable, the independent variable, using de _lastAction_ from the last loan.

    SELECT
     Customer,
     lastAction
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary
     WHERE
      lastAction IN ('Loan Redemption', 'Drop Loan') AND
      DATEPART(yyyy,OriginDate) = 2018               AND
      LoanCountry               = 'Mex'              
    ) aux
    WHERE
     rank = 1

## Store

Loans per store per customer

    WITH
    LOANSUMMARY AS
     (
      SELECT
       aux.*,
       cus.state AS customer_state
      FROM
      (
       SELECT
        *,
        ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
       FROM
        dbo.LoanSummary
       WHERE
        lastAction IN ('Loan Redemption', 'Drop Loan') AND
        DATEPART(yyyy,OriginDate) = 2018               AND
        LoanCountry               = 'Mex'              
      ) aux
      LEFT JOIN dbo.CustomerTable cus ON aux.Customer = cus.CustomerID
      WHERE
       rank > 1
     )

    SELECT
     DISTINCT
      lt.Customer,
      lt.PrimaryLoan,
      lt.store
    FROM
     dbo.LoanTable lt
    WHERE
     lt.PrimaryLoan IN (SELECT LS.PrimaryLoan FROM LOANSUMMARY LS)

# Data Extraction

## LoanSummary & Customer features


```python
loan_cust=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/LoanSummary_CustomerTable_features_2018.csv")
```


```python
loan_cust.columns
```




    Index(['Customer', 'total_loans', 'total_loan_redemption', 'total_drop_loan',
           'total_Originalloan_loan_redemption', 'total_Originalloan_drop_loan',
           'total_RenewalDollars_loan_redemption',
           'total_RenewalDollars_drop_loan', 'total_items', 'avg_items_per_loan',
           'sd_items_per_loan', 'max_items_per_loan', 'total_originalloan',
           'avg_originalloan_per_loan', 'sd_originalloan_per_loan',
           'total_extensions', 'avg_extensions_per_loan', 'sd_extensions_per_loan',
           'min_extensions_per_loan', 'max_extensions_per_loan',
           'total_extensiondollars', 'avg_extensiondollars_per_loan',
           'sd_extensiondollars_per_loan', 'total_renewals', 'avg_renewals',
           'max_renewals', 'min_renewals', 'total_RenewalDollars',
           'avg_RenewalDollars', 'sd_RenewalDollars',
           'avg_pct_extensiondollars_originalloan_per_loan',
           'avg_pct_RenewalDollars_originalloan_per_loan', 'Gender', 'City',
           'state', 'birth_date', 'zip'],
          dtype='object')



## Category features


```python
catg=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/Category_Features_2018.csv")
```


```python
catg.columns
```




    Index(['Customer', 'Tools', 'Electronics', 'Household_Office_Goods',
           'Musical_Instruments', 'Sporting_Goods', 'Transportation', 'Bracelet',
           'Ring', 'Chain', 'Yard_Equipment', 'Camera_Equipment', 'Pendant_Locket',
           'Gun_Police_Equipment', 'Earrings', 'Hand_Guns', 'Long_Guns',
           'Money_Clip', 'Watch', 'Other'],
          dtype='object')



## LoanSummary days features


```python
loan_days=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/LoanSummary_days_features_2018.csv")
```


```python
loan_days.columns
```




    Index(['Customer', 'avg_days_for_loan', 'sd_days_for_loan',
           'max_days_for_loan', 'min_days_for_loan', 'dif_max_avg_days_for_loan',
           'dif_avg_min_days_for_loan'],
          dtype='object')



## Y


```python
y_data=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/y_lastAction_2018.csv")
```


```python
y_data.columns
```




    Index(['Customer', 'lastAction'], dtype='object')



## Loan per Store per customer


```python
store_loan_data=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/Store_per_loan_customer.csv")
```


```python
store_loan_data.columns
```




    Index(['Customer', 'PrimaryLoan', 'store'], dtype='object')



## Store Distribution (Mexico)

Luis David (Category Manager) has a mexican Store Distribution in an Excel. This is the one we are going to use to know the state of each store, joining this table with the store number per loan.


```python
store_dist=pd.read_excel("/Users/darias/Documents/PUR_renewal_nse_model/Data/Distribución Nov FY19_2.xlsx", sheet = "Distribución")
```


```python
store_dist.columns
```




    Index(['Store 1', 'Nombre', 'Distrito', 'Región', 'Estado', 'Municipio/Ciudad',
           'Apertura', 'Tipo Tienda', 'Distrito.1', 'Región.1'],
          dtype='object')



## Socioeconomic Level

Table with Urban or Rural socioeconomic Level for each Zip code


```python
nse=pd.read_csv("/Users/darias/Documents/PUR_renewal_nse_model/Data/nse_cod_postal.csv")
```


```python
nse.columns
```




    Index(['codigo_postal', 'nse', 'nse_rural'], dtype='object')



# Store Features

Steps:

  1. Join the store per loan with the store distribution
  2. Join with the customer state
  3. Make the features:
    - Pct of loans in the residence state
    - Pct of loans for each mexican state

## Join Store per loan & store distribution

Change the name of the Store number


```python
store_dist.rename(columns={'Store 1':'store'}, inplace=True)
```

Keep only store and state from the store distribution


```python
store_dist=(
store_dist >>
    select(X.store, X.Estado)
)
```

Join with the store per loan


```python
store_loan_data=(
store_loan_data >>
    left_join(store_dist, by = 'store')
)
```


```python
(
store_loan_data >>
    head(2)
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer</th>
      <th>PrimaryLoan</th>
      <th>store</th>
      <th>Estado</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>529</td>
      <td>342672762</td>
      <td>85226</td>
      <td>Campeche</td>
    </tr>
    <tr>
      <th>1</th>
      <td>529</td>
      <td>340600671</td>
      <td>85303</td>
      <td>Campeche</td>
    </tr>
  </tbody>
</table>
</div>



## Join with Customer state residence

The table we create, is going to join with the customer state of residence, to know how many loans each customer did in their state of residence.


```python
store_loan_data=(
    store_loan_data >>
     left_join(loan_cust >> select(X.Customer, X.state), by = 'Customer')
)
```

## Store and Residence state in the same format


```python
store_loan_data=(
store_loan_data >>
    mask(X.Estado != '') >>
    mutate(Estado = X.Estado.replace('Aguascalientes', 'Aguascalientes')) >>
    mutate(Estado = X.Estado.replace('Campeche', 'Campeche')) >>
    mutate(Estado = X.Estado.replace('Chiapas', 'Chiapas')) >>
    mutate(Estado = X.Estado.replace('Coahuila', 'Coahuila')) >>
    mutate(Estado = X.Estado.replace('D.F.', 'Distrito Federal')) >>
    mutate(Estado = X.Estado.replace('Edomex', 'Mexico')) >>
    mutate(Estado = X.Estado.replace('Guanajuato', 'Guanajuato')) >>
    mutate(Estado = X.Estado.replace('Guerrero', 'Guerrero')) >>
    mutate(Estado = X.Estado.replace('Hidalgo', 'Hidalgo')) >>
    mutate(Estado = X.Estado.replace('Jalisco', 'Jalisco')) >>
    mutate(Estado = X.Estado.replace('Michoacán', 'Michoacan De Ocampo')) >>
    mutate(Estado = X.Estado.replace('Morelos', 'Morelos')) >>
    mutate(Estado = X.Estado.replace('Nuevo León', 'Nuevo Leon')) >>
    mutate(Estado = X.Estado.replace('Oaxaca', 'Oaxaca')) >>
    mutate(Estado = X.Estado.replace('Puebla', 'Puebla')) >>
    mutate(Estado = X.Estado.replace('Querétaro', 'Queretaro')) >>
    mutate(Estado = X.Estado.replace('Quintana Roo', 'Quintana Roo')) >>
    mutate(Estado = X.Estado.replace('San Luis Potosí', 'San Luis Potosi')) >>
    mutate(Estado = X.Estado.replace('Sinaloa', 'Sinaloa')) >>
    mutate(Estado = X.Estado.replace('Tabasco', 'Tabasco')) >>
    mutate(Estado = X.Estado.replace('Tamaulipas', 'Tamaulipas')) >>
    mutate(Estado = X.Estado.replace('Tlaxcala', 'Tlaxcala')) >>
    mutate(Estado = X.Estado.replace('Veracruz', 'Veracruz'))
)
```


```python
store_loan_data=store_loan_data.dropna(subset=['Estado'])
```

## Pct of Loans in the state of residence


```python
pct_loans_residence_state=(
store_loan_data >>
    mutate(ind = if_else(X.Estado == X.state, 1, 0)) >>
    group_by(X.Customer) >>
    summarize(pct_loans_residence_state = X.ind.sum()/n(X.Customer))
)
```


```python
(
pct_loans_residence_state >>
    head(2)
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer</th>
      <th>pct_loans_residence_state</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>529</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>44234</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>65131</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>65650</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>92030</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>236501</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>242836</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>248094</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>363701</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>602585</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>613707</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>11</th>
      <td>619775</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>12</th>
      <td>622153</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>809615</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>14</th>
      <td>892959</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>898970</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>16</th>
      <td>1118442</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>1171619</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>18</th>
      <td>1222327</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>19</th>
      <td>2165454</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2286849</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>21</th>
      <td>2340317</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>22</th>
      <td>2364927</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>23</th>
      <td>2404197</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>24</th>
      <td>2404312</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>25</th>
      <td>2408487</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>2410188</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>27</th>
      <td>2413348</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>28</th>
      <td>2413503</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>29</th>
      <td>2414245</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>293697</th>
      <td>500645009</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293698</th>
      <td>500645025</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293699</th>
      <td>500645060</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293700</th>
      <td>500645062</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293701</th>
      <td>500645066</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293702</th>
      <td>500645501</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293703</th>
      <td>500645509</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293704</th>
      <td>500645515</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293705</th>
      <td>500645520</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293706</th>
      <td>500645527</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293707</th>
      <td>500645531</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293708</th>
      <td>500645535</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293709</th>
      <td>500645540</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293710</th>
      <td>500645545</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293711</th>
      <td>500645547</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293712</th>
      <td>500645550</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293713</th>
      <td>500645551</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293714</th>
      <td>500645559</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293715</th>
      <td>500645566</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293716</th>
      <td>500645573</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293717</th>
      <td>500645589</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293718</th>
      <td>500645591</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293719</th>
      <td>500645610</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293720</th>
      <td>500645614</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293721</th>
      <td>500645628</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293722</th>
      <td>500645644</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293723</th>
      <td>500645670</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>293724</th>
      <td>500645680</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293725</th>
      <td>500645684</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>293726</th>
      <td>500645694</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
<p>293727 rows × 2 columns</p>
</div>



## Pct of loans per state

Percentage of loans per state of the stores. The make a spread to make columns of each state


```python
store_state_pct=(
store_loan_data >>
    group_by(X.Customer, X.Estado) >>
    summarize(n = n(X.Customer)) >>
    group_by(X.Customer) >>
    mutate(total = X.n.sum()) >>
    mutate(pct = X.n/X.total)
).pivot(index='Customer', columns='Estado', values='pct')
```


```python
store_state_pct=store_state_pct.fillna(0)
```


```python
store_state_pct.columns=store_state_pct.columns.str.replace(' ','')
```


```python
store_state_pct.columns='pct_store_'+store_state_pct.columns
```


```python
(
store_state_pct >>
    head(2)
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>pct_store_Aguascalientes</th>
      <th>pct_store_Campeche</th>
      <th>pct_store_Chiapas</th>
      <th>pct_store_Coahuila</th>
      <th>pct_store_DistritoFederal</th>
      <th>pct_store_Guanajuato</th>
      <th>pct_store_Guerrero</th>
      <th>pct_store_Hidalgo</th>
      <th>pct_store_Jalisco</th>
      <th>pct_store_Mexico</th>
      <th>...</th>
      <th>pct_store_Oaxaca</th>
      <th>pct_store_Puebla</th>
      <th>pct_store_Queretaro</th>
      <th>pct_store_QuintanaRoo</th>
      <th>pct_store_SanLuisPotosi</th>
      <th>pct_store_Sinaloa</th>
      <th>pct_store_Tabasco</th>
      <th>pct_store_Tamaulipas</th>
      <th>pct_store_Tlaxcala</th>
      <th>pct_store_Veracruz</th>
    </tr>
    <tr>
      <th>Customer</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>529</th>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>44234</th>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>2 rows × 23 columns</p>
</div>



## Join all store features


```python
store_features=(
pct_loans_residence_state >>
  left_join(store_state_pct, by = 'Customer')
)
```

# Socioeconomic Level

Join the socioeconomic Level by the customer Zip Code to the LoanSummary & Customer Features


```python
# Drop Zip Codes with unknown socioeconomic level
nse=(
    nse >>
    mask(X.nse != 'NC') >>
    mask(X.nse_rural != 'NC')
)
```

Add a label to the socioeconomic level to differentiate between urban and rural when the join went done


```python
nse=(
nse >>
    mutate(nse       = X.nse+' - urban',
           nse_rural = X.nse_rural+' - rural')
)
```


```python
# Unique Zip codes
nse=(
nse >>
    distinct(X.codigo_postal)
)
```

Create just 1 column with socioeconomic level


```python
nse=(
nse.fillna('') >>
    mutate(sel = X.nse+X.nse_rural) >>
    select(X.codigo_postal, X.sel)
)
```

Change column name to english


```python
nse.rename(columns={'codigo_postal':'zip'}, inplace=True)
```

Join the SEL with the LoanSummary & Customer features


```python
#loan_cust=(
#loan_cust >>
#    left_join(nse, by = 'zip')
#)
```

# Data Cleaning

##  Loan & Customer features

Just customers with at least 3 loans


```python
loan_cust=(
loan_cust >>
    mask(X.total_loans>=3)
)
```

## Days for next loan features


```python
loan_days=loan_days.fillna(0)
```

## Y


```python
y_data=(
y_data >>
    mutate(y_red=if_else(X.lastAction=='Loan Redemption', 1, 0))
)
```

# Joining Data

Joining all the tables to create the complete data


```python
datas=(loan_cust >>
  left_join(loan_days,      by = 'Customer') >>
  left_join(catg     ,      by = 'Customer') >>
  left_join(y_data   ,      by = 'Customer') >>
  left_join(store_features, by = 'Customer')
)
```


```python
datas=datas.dropna(subset=['pct_loans_residence_state'])
```


```python
datas=datas.dropna(subset=['lastAction'])
```

# Data Engineering

Features to create:

  - Pct of each items per category
  - Pct of drop loans & loan redemption
  - Age
  - Clasify in north, center and south mexico each state
  - PUR
  - Pct of money dropped & redeemed

Delete rows with age greater than 80


```python
datas['birth_date']=pd.to_datetime(datas['birth_date'])
```


```python
datas['age']=(dt.datetime.now()-datas['birth_date']).dt.days
```


```python
datas=(
datas >>
    mutate(pct_drop_loan              = X.total_drop_loan/X.total_loans,
           pct_loan_redemption        = X.total_loan_redemption/X.total_loans,
           pct_Tools                  = X.Tools/X.total_items,
           pct_Electronics            = X.Electronics/X.total_items,
           pct_Household_Office_Goods = X.Household_Office_Goods/X.total_items,
           pct_Musical_Instruments    = X.Musical_Instruments/X.total_items,
           pct_Sporting_Goods         = X.Sporting_Goods/X.total_items,
           pct_Transportation         = X.Transportation/X.total_items,
           pct_Bracelet               = X.Bracelet/X.total_items,
           pct_Ring                   = X.Ring/X.total_items,
           pct_Chain                  = X.Chain/X.total_items,
           pct_Yard_Equipment         = X.Yard_Equipment/X.total_items,
           pct_Camera_Equipment       = X.Camera_Equipment/X.total_items,
           pct_Pendant_Locket         = X.Pendant_Locket/X.total_items,
           pct_Gun_Police_Equipment   = X.Gun_Police_Equipment/X.total_items,
           pct_Earrings               = X.Earrings/X.total_items,
           pct_Hand_Guns              = X.Hand_Guns/X.total_items,
           pct_Long_Guns              = X.Long_Guns/X.total_items,
           pct_Money_Clip             = X.Money_Clip/X.total_items,
           pct_Watch                  = X.Watch/X.total_items,
           pct_Other                  = X.Other/X.total_items,
           age                        = (X.age/365).round(),
           pct_money_drop             = X.total_Originalloan_drop_loan/X.total_originalloan,
           pct_money_redeem           = X.total_Originalloan_loan_redemption/X.total_originalloan,
           pur                        = X.total_Originalloan_loan_redemption/(X.total_Originalloan_loan_redemption+X.total_Originalloan_drop_loan))
)
```


```python
datas=(
    datas >>
    mask(X.age <= 80)
)
```


```python
mex_states=[
'Veracruz',
'Distrito Federal',
'Mexico',
'Tabasco',
'Jalisco',
'Campeche',
'Puebla',
'Guanajuato',
'Sinaloa',
'Hidalgo',
'Chiapas',
'Michoacan De Ocampo',
'Tamaulipas',
'Guerrero',
'Queretaro',
'Tlaxcala',
'Quintana Roo',
'Oaxaca',
'Nuevo Leon',
'Aguascalientes',
'Morelos',
'Coahuila',
'San Luis Potosi',
'Baja California',
'Yucatan',
'Nayarit',
'Durango',
'Chihuahua',
'Sonora',
'Zacatecas',
'Baja California Sur',
'Colima'
]
```


```python
datas=(
datas >>
    mask(X.state.isin(mex_states))
)
```


```python
north=['Sinaloa', 'Tamaulipas', 'Nuevo Leon', 'Coahuila', 'San Luis Potosi',
       'Baja California', 'Nayarit', 'Durango', 'Chihuahua', 'Sonora',
       'Zacatecas', 'Baja California Sur', 'Colima']

center=['Veracruz', 'Distrito Federal', 'Mexico', 'Jalisco',
        'Puebla', 'Guanajuato', 'Hidalgo', 'Michoacan De Ocampo',
        'Queretaro', 'Aguascalientes', 'Morelos']

south=['Tabasco', 'Campeche', 'Chiapas', 'Guerrero', 'Tlaxcala', 'Quintana Roo', 'Oaxaca', 'Yucatan']
```


```python
datas=(
datas >>
    mutate(ind_geo = if_else(X.state.isin(north), 'north',
                            if_else(X.state.isin(center), 'center',
                                   if_else(X.state.isin(south), 'south', 'NI'))))
)
```


```python
datas=datas.dropna(subset=['avg_days_for_loan'])
```


```python
datas.columns.tolist()
```




    ['Customer',
     'total_loans',
     'total_loan_redemption',
     'total_drop_loan',
     'total_Originalloan_loan_redemption',
     'total_Originalloan_drop_loan',
     'total_RenewalDollars_loan_redemption',
     'total_RenewalDollars_drop_loan',
     'total_items',
     'avg_items_per_loan',
     'sd_items_per_loan',
     'max_items_per_loan',
     'total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'total_extensions',
     'avg_extensions_per_loan',
     'sd_extensions_per_loan',
     'min_extensions_per_loan',
     'max_extensions_per_loan',
     'total_extensiondollars',
     'avg_extensiondollars_per_loan',
     'sd_extensiondollars_per_loan',
     'total_renewals',
     'avg_renewals',
     'max_renewals',
     'min_renewals',
     'total_RenewalDollars',
     'avg_RenewalDollars',
     'sd_RenewalDollars',
     'avg_pct_extensiondollars_originalloan_per_loan',
     'avg_pct_RenewalDollars_originalloan_per_loan',
     'Gender',
     'City',
     'state',
     'birth_date',
     'zip',
     'avg_days_for_loan',
     'sd_days_for_loan',
     'max_days_for_loan',
     'min_days_for_loan',
     'dif_max_avg_days_for_loan',
     'dif_avg_min_days_for_loan',
     'Tools',
     'Electronics',
     'Household_Office_Goods',
     'Musical_Instruments',
     'Sporting_Goods',
     'Transportation',
     'Bracelet',
     'Ring',
     'Chain',
     'Yard_Equipment',
     'Camera_Equipment',
     'Pendant_Locket',
     'Gun_Police_Equipment',
     'Earrings',
     'Hand_Guns',
     'Long_Guns',
     'Money_Clip',
     'Watch',
     'Other',
     'lastAction',
     'y_red',
     'pct_loans_residence_state',
     'pct_store_Aguascalientes',
     'pct_store_Campeche',
     'pct_store_Chiapas',
     'pct_store_Coahuila',
     'pct_store_DistritoFederal',
     'pct_store_Guanajuato',
     'pct_store_Guerrero',
     'pct_store_Hidalgo',
     'pct_store_Jalisco',
     'pct_store_Mexico',
     'pct_store_MichoacanDeOcampo',
     'pct_store_Morelos',
     'pct_store_NuevoLeon',
     'pct_store_Oaxaca',
     'pct_store_Puebla',
     'pct_store_Queretaro',
     'pct_store_QuintanaRoo',
     'pct_store_SanLuisPotosi',
     'pct_store_Sinaloa',
     'pct_store_Tabasco',
     'pct_store_Tamaulipas',
     'pct_store_Tlaxcala',
     'pct_store_Veracruz',
     'age',
     'pct_drop_loan',
     'pct_loan_redemption',
     'pct_Tools',
     'pct_Electronics',
     'pct_Household_Office_Goods',
     'pct_Musical_Instruments',
     'pct_Sporting_Goods',
     'pct_Transportation',
     'pct_Bracelet',
     'pct_Ring',
     'pct_Chain',
     'pct_Yard_Equipment',
     'pct_Camera_Equipment',
     'pct_Pendant_Locket',
     'pct_Gun_Police_Equipment',
     'pct_Earrings',
     'pct_Hand_Guns',
     'pct_Long_Guns',
     'pct_Money_Clip',
     'pct_Watch',
     'pct_Other',
     'pct_money_drop',
     'pct_money_redeem',
     'pur',
     'ind_geo']



# Descriptive Analysis

Descriptives...

## Drop & Redeem totals


```python
 (datas >>
  group_by(X.lastAction) >>
  summarize(n = n(X.lastAction)) >>
  ggplot() +
  geom_bar(aes(x = 'lastAction', y = 'n'), stat = 'identity', fill = 'darkblue') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'n', label = 'n'), va='bottom',format_string='{:,}') +
  scale_y_continuous(labels = comma_format()) +
  ggtitle('Drop vs No Drop Counts')
)
```


![png](output_120_0.png)





    <ggplot: (7550302812)>



### Drop & Redeem Pct


```python
(datas >>
  group_by(X.lastAction) >>
  summarize(pct = n(X.lastAction)/len(datas)) >>
  ggplot() +
  geom_bar(aes(x = 'lastAction', y = 'pct'), stat = 'identity', fill = 'darkblue') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'pct', label = '(pct*100).round(2)'), va='bottom',format_string='{}%') +
  scale_y_continuous(labels = percent_format()) +
  ggtitle('Drop vs No Drop Pct')
)
```


![png](output_122_0.png)





    <ggplot: (-9223372029228197916)>



## PUR

### Histogram


```python
(
datas >>
  ggplot() +
  geom_histogram(aes('pur'), fill = 'darkblue') +
  scale_x_continuous(labels = percent_format()) +
  scale_y_continuous(labels = comma_format()) +
  ggtitle('PUR Histogram') +
  theme_bw() +
  geom_vline(xintercept = median(datas['pur'])) +
  annotate(geom  = 'text',
           x     = median(datas['pur']),
           y     = 20000,
           label = 'Median ='+str(round(median(datas['pur']),4)*100)+'%')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 39'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))



![png](output_125_1.png)





    <ggplot: (7598353044)>



### PUR median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarize(med = median(X.pur)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('PUR median - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med,4)*100'), va = 'bottom', format_string='{}%') +
  ylab('PUR') +
  xlab('') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_127_0.png)





    <ggplot: (-9223372029254743229)>



### PUR Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'pur')) +
    theme_bw() +
    ggtitle('PUR boxplot - Drop vs Redeem') +
    scale_y_continuous(labels = percent_format())
)
```


![png](output_129_0.png)





    <ggplot: (-9223372029187484521)>



### PUR median - Male vs Female


```python
(
datas >>
  group_by(X.Gender) >>
  summarize(med = median(X.pur)) >>
  ggplot() +
  geom_col(aes(x = 'Gender', y = 'med'), fill = 'darkblue') +
  ggtitle('PUR median - Gender') +
  theme_bw() +
  geom_text(aes(x = 'Gender', y = 'med', label = 'round(med*100,2)'), va = 'bottom', format_string='{}%') +
  ylab('PUR') +
  xlab('') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_131_0.png)





    <ggplot: (7601571115)>



### PUR median - Geographical Indicator


```python
(
datas >>
  group_by(X.ind_geo) >>
  summarize(med = median(X.pur)) >>
  ggplot() +
  geom_col(aes(x = 'ind_geo', y = 'med'), fill = 'darkblue') +
  ggtitle('PUR median - Geo Indicator') +
  theme_bw() +
  geom_text(aes(x = 'ind_geo', y = 'med', label = 'round(med*100,2)'), va = 'bottom', format_string='{}%') +
  ylab('PUR') +
  xlab('') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_133_0.png)





    <ggplot: (-9223372029244491502)>



### PUR median - Customers Residence State


```python
order_state=(
datas >>
  group_by(X.state) >>
  summarize(med = median(X.pur))
).sort_values(by=['med'])['state'].tolist()

(
datas >>
  group_by(X.state) >>
  summarise(med = median(X.pur)) >>
  arrange(X.med) >>
  ggplot() +
  geom_col(aes(x = 'state', y = 'med'), fill = 'darkblue') +
  ggtitle('PUR median - State') +
  theme_bw() +
  geom_text(aes(x = 'state', y = 'med', label = 'round(med*100,1)'),
            va            = 'bottom',
            format_string = '{}%',
            angle         = 90) +
  ylab('PUR') +
  xlab('') +
  theme(axis_text_x = element_text(angle = 90)) +
  scale_x_discrete(limits = order_state) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_135_0.png)





    <ggplot: (7624778437)>



### PUR median - Age


```python
order_age=(
datas.sort_values(by=['age']) >>
    mask(X.age<=80) >>
    select(X.age) >>
    mutate(age=X.age.round().astype('str')) >>
    distinct()
).sort_values(by=['age'])['age'].tolist()

(
datas >>
  mask(X.age<=80) >>
  mutate(age=X.age.astype('str')) >>
  group_by(X.age) >>
  summarize(med = median(X.pur)) >>
  ggplot() +
  geom_line(aes(x = 'age', y = 'med', group = 1), colour = 'darkblue') +
  ggtitle('PUR median - Age') +
  theme_bw() +
  geom_text(aes(x = 'age', y = 'med', label = 'round(med*100,1)'),
            va            = 'bottom',
            format_string = '{}%',
            angle         = 90,
            size          = 6) +
  ylab('PUR') +
  xlab('') +
  theme(axis_text_x = element_text(angle = 90, size = 6)) +
  scale_x_discrete(limits = order_age) +
  scale_y_continuous(labels = percent_format())
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:517: MatplotlibDeprecationWarning: isinstance(..., numbers.Number)
      return not cbook.iterable(value) and (cbook.is_numlike(value) or



![png](output_137_1.png)





    <ggplot: (7638399997)>



### Redeem Efectiveness

We see two important thing:

  - The 10% worst PUR is 0
  - The 20% best PUR is 0

These means, that we cannot segment the 10% worst and the 20% best PUR.


```python
datas['pur'].quantile(q=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
```




    0.1    0.000000
    0.2    0.212121
    0.3    0.350000
    0.4    0.474576
    0.5    0.594677
    0.6    0.703704
    0.7    0.824561
    0.8    1.000000
    0.9    1.000000
    1.0    1.000000
    Name: pur, dtype: float64




```python
datas['per']=pd.qcut(datas.pur, q=[0,0.2,0.3,0.4,0.5,
                                       0.6,0.7,1.0], 
                                   labels=['20%','30%','40%','50%','60%','70%','100%'])
```


```python
(
 datas >>
    mutate(obs = if_else(X.lastAction=='Loan Redemption',1,0)) >>
    group_by(X.per) >>
    summarize(n = n(X.obs),
              vp = X.obs.sum()) >>
    mutate(eff = X.vp/X.n*100) >>
    mutate(eff = X.eff.round(2)) >>
    ggplot() +
    geom_bar(aes(x = 'per', y = 'eff'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    ggtitle('Redeem Efectiveness per quantiles - PUR') +
    theme(axis_text_x  = element_text(size = 13, face = 'bold'),
          axis_text_y  = element_text(size = 13, face = 'bold'),
          axis_title_x = element_text(size = 13, face = 'bold'),
          axis_title_y = element_text(size = 13, face = 'bold')) +
    geom_text(aes(x='per',y='eff',label='eff'),va='bottom',size=8,format_string='{}%') +
    geom_hline(yintercept=sum(datas['y_red'])/len(datas)*100) +
    annotate(geom  = 'text',
             x     = '40%',
             y     = sum(datas['y_red'])/len(datas)*100,
             label = '%Redeem= '+str(round(sum(datas['y_red'])/len(datas)*100,2))+'%',
             ha    = 'right',
             va    = 'bottom')
)
```


![png](output_142_0.png)





    <ggplot: (-9223372029256420990)>



## Gender

### Distribution


```python
(
datas >>
  group_by(X.Gender) >>
  summarise(n = n(X.Gender)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'Gender', y = 'n'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Gender Pct') +
  geom_text(aes(x='Gender',y='n',label='round(n*100,2)'),va='bottom',size=8,format_string='{}%') +
  xlab('') +
  ylab('') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_145_0.png)





    <ggplot: (7651451989)>



### Redeem pct


```python
(datas >>
  group_by(X.Gender, X.lastAction) >>
  summarise(n = n(X.Gender)) >>
  group_by(X.Gender) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'Gender', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop Pct - Gender') +
  geom_text(aes(x = 'Gender', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_147_0.png)





    <ggplot: (7667290849)>



## Geographical Indicator

### Distirbution


```python
order_ind_geo=(
datas >>
  group_by(X.ind_geo) >>
  summarise(n = n(X.ind_geo)/len(datas))
).sort_values(by=['n'])['ind_geo'].tolist()

(
datas >>
  group_by(X.ind_geo) >>
  summarise(n = n(X.ind_geo)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'ind_geo', y = 'n'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Geographical Indicator Pct') +
  geom_text(aes(x = 'ind_geo', y = 'n', label = 'round(n*100,2)'), va='bottom', format_string='{}%') +
  xlab('') +
  ylab('') +
  scale_x_discrete(limits = order_ind_geo) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_150_0.png)





    <ggplot: (-9223372029240410887)>



### Redeem Pct


```python
(
datas >>
  group_by(X.ind_geo, X.lastAction) >>
  summarise(n = n(X.ind_geo)) >>
  group_by(X.ind_geo) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'ind_geo', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop Pct - Geographical Indicator') +
  geom_text(aes(x = 'ind_geo', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_152_0.png)





    <ggplot: (-9223372029244482602)>



## Total Loans

### Distribution


```python
(
datas >>
  ggplot() +
  geom_density(aes('total_loans'), fill = 'darkblue') +
  ggtitle('Total Loans Distribution') +
  theme_bw() +
  scale_x_continuous(limits = [0,50]) +
  geom_vline(xintercept=median(datas['total_loans'])) +
  annotate(geom  = 'text',
           x     = median(datas['total_loans']),
           y     = 0.25,
           label = 'Median= '+str(median(datas['total_loans'])),
           ha    = 'left',
           va    = 'bottom')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_density : Removed 223 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_155_1.png)





    <ggplot: (-9223372029244482609)>



### Median Loans - Drop vs Redeem


```python
(
  datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.total_loans)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Total loans median - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom', format_string='{}') +
  ylab('Loans') +
  xlab('')
)
```


![png](output_157_0.png)





    <ggplot: (7600036623)>



## Pct of Redeem

### Distribution


```python
(
datas >>
  ggplot() +
  geom_density(aes('pct_loan_redemption'), fill = 'darkblue') +
  ggtitle('Pct Loan Redemption') +
  theme_bw() +
  scale_x_continuous(limits = [0,1]) +
  geom_vline(xintercept=median(datas['pct_loan_redemption'])) +
  annotate(geom  = 'text',
           x     = median(datas['pct_loan_redemption']),
           y     = 1.7,
           label = 'Median= '+str(median(datas['pct_loan_redemption'])),
           ha    = 'left',
           va    = 'bottom') +
  scale_x_continuous(labels = percent_format())
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/scales/scales.py:28: UserWarning: Scale for 'x' is already present.
    Adding another scale for 'x',
    which will replace the existing scale.
    
      warn(_TPL_DUPLICATE_SCALE.format(ae))



![png](output_160_1.png)





    <ggplot: (7610441322)>



### Median - Drop vs Redeem


```python
(
 datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.pct_loan_redemption)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Pct of Loans Redemptions - For Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med*100,2)'), va='bottom', format_string='{}%')+
  scale_y_continuous(labels = percent_format())
)
```


![png](output_162_0.png)





    <ggplot: (-9223372029256422733)>



## Average Original Loan

### Distribution


```python
(
datas >>
  ggplot() +
  geom_histogram(aes('avg_originalloan_per_loan'), fill = 'darkblue') +
  ggtitle('Average Original Loan - Distribution') +
  theme_bw() +
  scale_x_continuous(limits =[0,2000]) +
  geom_vline(xintercept = median(datas['avg_originalloan_per_loan'])) +
  annotate(geom  = 'text',
            x     = median(datas['avg_originalloan_per_loan']),
            y     = 3000,
            label = 'Median = $'+str(round(median(datas['avg_originalloan_per_loan']),0)),
            ha    = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 59'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_bin : Removed 11465 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_165_1.png)





    <ggplot: (-9223372029255255783)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.avg_originalloan_per_loan)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Average Original Loan - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_167_0.png)





    <ggplot: (7667294016)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'avg_originalloan_per_loan')) +
    theme_bw() +
    scale_y_continuous(limits=[0,10000]) +
    ggtitle('Boxplot avg original loan - Drop vs Redeem')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_boxplot : Removed 261 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_169_1.png)





    <ggplot: (-9223372029241637253)>



## Standard Deviation Original Loan

### Distribution


```python
(
datas >>
  ggplot() +
  geom_histogram(aes('sd_originalloan_per_loan'), fill = 'darkblue') +
  ggtitle('Sd Original Loan - Distribution') +
  theme_bw() +
  scale_x_continuous(limits =[0,2000]) +
  geom_vline(xintercept = median(datas['sd_originalloan_per_loan'])) +
  annotate(geom  = 'text',
            x     = median(datas['sd_originalloan_per_loan']),
            y     = 3000,
            label = 'Median = $'+str(round(median(datas['sd_originalloan_per_loan']),0)),
            ha    = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 93'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_bin : Removed 3717 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_172_1.png)





    <ggplot: (7598346479)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.sd_originalloan_per_loan)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Sd Original Loan - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_174_0.png)





    <ggplot: (-9223372029232784310)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'sd_originalloan_per_loan')) +
    theme_bw() +
    scale_y_continuous(limits=[0,500]) +
    ggtitle('Boxplot sd Original Loan - Drop vs Redeem')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_boxplot : Removed 37254 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_176_1.png)





    <ggplot: (-9223372029187479110)>



### Scatter - Sd Original Loan vs Pct of Redeem


```python
(
datas >>
    ggplot() +
    geom_point(aes(x = 'sd_originalloan_per_loan', y = 'pct_loan_redemption')) +
    ggtitle('Sd original loan vs Pct of Redeem') +
    theme_bw() +
    facet_wrap('~lastAction') +
    scale_y_continuous(labels = percent_format())
)
```


![png](output_178_0.png)





    <ggplot: (7600030191)>



### Scatter - Sd original Loan vs Avg Original Loan


```python
(
datas >>
    ggplot() +
    geom_point(aes(x = 'sd_originalloan_per_loan', y = 'avg_originalloan_per_loan')) +
    theme_bw() +
    ggtitle('Sd original loan vs avg original loan') +
    facet_wrap('~lastAction') +
    scale_y_continuous(limits=[0,20000]) +
    scale_x_continuous(limits=[0,50000])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_point : Removed 77 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_180_1.png)





    <ggplot: (7596597154)>



## Total Renewals

### Distribution


```python
(
datas >>
    group_by(X.total_renewals) >>
    summarise(pct = n(X.total_renewals)/len(datas)) >>
    ggplot() +
    geom_col(aes(x = 'total_renewals', y = 'pct'), fill = 'darkblue') +
    theme_bw() +
    ggtitle('Total Renewals Distribution') +
    scale_x_continuous(limits=[-1,11]) +
    scale_y_continuous() +
    scale_y_continuous(labels = percent_format()) +
    geom_text(aes(x = 'total_renewals', y = 'pct', label = 'round(pct*100,1)'),va='bottom',format_string='{}%')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/scales/scales.py:28: UserWarning: Scale for 'y' is already present.
    Adding another scale for 'y',
    which will replace the existing scale.
    
      warn(_TPL_DUPLICATE_SCALE.format(ae))
    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:32: RuntimeWarning: invalid value encountered in reduce
      return umr_minimum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:28: RuntimeWarning: invalid value encountered in reduce
      return umr_maximum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:430: UserWarning: position_stack : Removed 108 rows containing missing values.
      data = self.position.setup_data(self.data, params)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_col : Removed 1 rows containing missing values.
      self.data = self.geom.handle_na(self.data)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_text : Removed 108 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_183_1.png)





    <ggplot: (-9223372029244335728)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'total_renewals')) + 
    theme_bw() +
    ggtitle('Total Renewals Boxplot - Drop vs Redeem') +
    scale_y_continuous(limits=[0,50])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_boxplot : Removed 242 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_185_1.png)





    <ggplot: (-9223372029256423585)>



## Average Renewals per Loan

### Distribution


```python
(
datas >>
    group_by(X.avg_renewals) >>
    summarise(pct = n(X.avg_renewals)/len(datas)) >>
    ggplot() +
    geom_col(aes(x = 'avg_renewals', y = 'pct'), fill = 'darkblue') +
    theme_bw() +
    ggtitle('Avg Renewals Distribution') +
    scale_x_continuous(limits=[-1,11]) +
    scale_y_continuous() +
    scale_y_continuous(labels = percent_format()) +
    geom_text(aes(x = 'avg_renewals', y = 'pct', label = 'round(pct*100,1)'),va='bottom',format_string='{}%')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/scales/scales.py:28: UserWarning: Scale for 'y' is already present.
    Adding another scale for 'y',
    which will replace the existing scale.
    
      warn(_TPL_DUPLICATE_SCALE.format(ae))
    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:32: RuntimeWarning: invalid value encountered in reduce
      return umr_minimum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:28: RuntimeWarning: invalid value encountered in reduce
      return umr_maximum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:430: UserWarning: position_stack : Removed 1 rows containing missing values.
      data = self.position.setup_data(self.data, params)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_col : Removed 1 rows containing missing values.
      self.data = self.geom.handle_na(self.data)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_text : Removed 1 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_188_1.png)





    <ggplot: (7546997873)>



### Mean - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = mean(X.avg_renewals)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Mean of Avg Renewals per loan - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med,2)'), va='bottom')
)
```


![png](output_190_0.png)





    <ggplot: (-9223372029240713037)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'avg_renewals')) + 
    theme_bw() +
    ggtitle('Avg Renewals Boxplot - Drop vs Redeem') +
    scale_y_continuous(limits=[0,10])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_boxplot : Removed 2 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_192_1.png)





    <ggplot: (7546997915)>



## Age

### Distribution


```python
(datas >>
  ggplot() +
  geom_histogram(aes('age'), fill = 'darkblue') + 
  ggtitle('Age Distribution') +
  theme_bw() +
  scale_x_continuous(limits = [17,80]) +
  geom_vline(xintercept = median(datas['age'])) +
  annotate(geom   = 'text',
           x      = median(datas['age']),
           y      = 3000,
           label  = 'Median = '+str(median(datas['age']))+' years',
           ha     = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 85'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_195_1.png)





    <ggplot: (7651453974)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'age')) +
    theme_bw() +
    ggtitle('Boxplot Age - Drop vs Redeem')
)
```


![png](output_197_0.png)





    <ggplot: (7599521572)>



## Average Days for Next Loan


```python
(datas >>
  ggplot() +
  geom_histogram(aes('avg_days_for_loan'), fill = 'darkblue') + 
  ggtitle('Avg days for next Loan Distribution') +
  theme_bw() +
  scale_x_continuous(limits = [-0.001,100]) +
  geom_vline(xintercept = median(datas['avg_days_for_loan'])) +
  annotate(geom   = 'text',
           x      = median(datas['avg_days_for_loan']),
           y      = 3000,
           label  = 'Median = '+str(median(datas['avg_days_for_loan']))+' days',
           ha     = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 69'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_bin : Removed 4234 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_199_1.png)





    <ggplot: (7601570161)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.avg_days_for_loan)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median Avg days for next Loan - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med,2)'), va='bottom')
)
```


![png](output_201_0.png)





    <ggplot: (7614366135)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'avg_days_for_loan')) +
    ggtitle('Boxplot Avg days for next Loan - Drop vs Redeem') +
    theme_bw() +
    scale_y_continuous(limits = [0,100])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_boxplot : Removed 4234 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)



![png](output_203_1.png)





    <ggplot: (7612881051)>



## Pct loans in Residence State


```python
(datas >>
  mutate(ind = if_else(X.pct_loans_residence_state == 1, 'All loans in residence', 'Not All loans in residence')) >>
  group_by(X.ind) >>
  summarize(pct = n(X.ind)/len(datas)) >>
  ggplot() +
  geom_bar(aes(x = 'ind', y = 'pct'), stat = 'identity', fill = 'darkblue') +
  ggtitle('Pct of customers with all loans in residence state') +
  theme_bw() +
  geom_text(aes(x = 'ind', y = 'pct', label = '(pct*100).round(2)'), va='bottom', format_string='{}%') +
  scale_y_continuous(labels = percent_format()) +
  xlab('')
)
```


![png](output_205_0.png)





    <ggplot: (7550390606)>



## Pct Money Redeem

### Distribution


```python
(datas >>
  ggplot() +
  geom_histogram(aes('pct_money_redeem'), fill = 'darkblue') + 
  ggtitle('Pct money redeem Distribution') +
  theme_bw() +
  scale_x_continuous(labels = percent_format()) +
  geom_vline(xintercept = median(datas['pct_money_redeem'])) +
  annotate(geom   = 'text',
           x      = median(datas['pct_money_redeem']),
           y      = 6000,
           label  = 'Median = '+str(round(median(datas['pct_money_redeem'])*100,2))+'%',
           ha     = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 39'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))



![png](output_208_1.png)





    <ggplot: (-9223372029232816310)>



# One Hot Encoding

Dummies for the categorical features:

 - State of Residence _(state)_
 - Geographical Indicator _(indgeo)_
 - Gender

Create dummies data frames


```python
state_dummies=pd.get_dummies(datas['state'])
ind_geo_dummies=pd.get_dummies(datas['ind_geo'])
gender_dummies=pd.get_dummies(datas['Gender'])
```

Fill Nan with 0


```python
state_dummies=state_dummies.fillna(0)
ind_geo_dummies=ind_geo_dummies.fillna(0)
gender_dummies=gender_dummies.fillna(0)
```

Join the dummies to the data


```python
datas=pd.concat([datas,state_dummies,ind_geo_dummies,gender_dummies],sort=False,axis=1)
```

Delete dummies data frames


```python
del(state_dummies)
del(ind_geo_dummies)
del(gender_dummies)
```

# Sample Data

A sample of 50% of the data to run the model faster


```python
datas=datas.sample(frac=0.5, replace=True, random_state=1)
```

# Redeem Model

## X & Y

Define features and variable to predict


```python
x=[feat for feat in datas.columns if feat not in ['Customer', 'Gender', 'City','state','birth_date',
                                                  'lastAction', 'y_red', 'ind_geo', 'per', 'zip']]
y='y_red'
```

## Split

Split the data into:
 - Train (balanced)
 - Test (unbalanced)
 - Valid (unbalanced)


```python
X=datas[x]
Y=datas[y]
```

Create Train and validation


```python
Xt,Xv,yt,yv = train_test_split(X,Y,train_size=0.8)
```

Split train, to balance one part and leave another unbalancet for test


```python
Xtrain, X_test, ytrain, ytest = train_test_split(Xt, yt, train_size = 0.8)
```

### Undersampling train


```python
# Separate majority and minority classes

# Majority class
x_data_majority = Xtrain.loc[ytrain == 1]
y_data_majority = ytrain.loc[ytrain == 1]

# Minority class
x_data_minority = Xtrain.loc[ytrain == 0]
y_data_minority = ytrain.loc[ytrain == 0]

# Downsample majority class
y_df_major_downsampled = sklearn.utils.resample(y_data_majority,replace=False,    # sample without replacement
                                n_samples=ytrain.value_counts()[0],  # to match minority class
                                random_state=10) # reproducible results

x_df_major_downsampled = sklearn.utils.resample(x_data_majority, replace=False,    # sample without replacement
                                n_samples=ytrain.value_counts()[0],  # to match minority class
                                random_state=10) # reproducible results

# Combine minority class with downsampled majority class
y_data_downsampled = pd.concat([y_df_major_downsampled, y_data_minority])
X_data_downsampled = pd.concat([x_df_major_downsampled, x_data_minority])

# Display new class counts
y_data_downsampled.value_counts()
```




    0.0    10209
    1.0    10209
    Name: y_red, dtype: int64



Now we have:
 - Train balanced _(ydatadownsampled, Xdatadownsampled)_
 - Test unbalanced _(Xtest, ytest)_
 - Valid unbalanced _(Xv, yv)_

## Hiperparameters


```python
params = {}
params['learning_rate'] = 0.2
params['boosting_type'] = 'gbdt'
params['objective'] = 'binary'
params['metric'] = 'binary_logloss'
params['sub_feature'] = 0.5
params['num_leaves'] = 300
params['min_data'] = 10
params['max_depth'] = 30
params['num_iterations'] = 300
```

## Fit the model


```python
import lightgbm as lgb
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/__init__.py:46: UserWarning: Starting from version 2.2.1, the library file in distribution wheels for macOS is built by the Apple Clang (Xcode_8.3.1) compiler.
    This means that in case of installing LightGBM from PyPI via the ``pip install lightgbm`` command, you don't need to install the gcc compiler anymore.
    Instead of that, you need to install the OpenMP library, which is required for running LightGBM on the system with the Apple Clang compiler.
    You can install the OpenMP library by the following command: ``brew install libomp``.
      "You can install the OpenMP library by the following command: ``brew install libomp``.", UserWarning)



```python
d_train = lgb.Dataset(X_data_downsampled, label=y_data_downsampled)
```


```python
clf = lgb.train(params, d_train, 10)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


## Accuracy

### Accuracy Test

Prediction


```python
y_pred=clf.predict(X_test)
for i in range(0,len(X_test)):
    if y_pred[i]>=.5:
       y_pred[i]=1
    else:
       y_pred[i]=0
```


```python
# Confussion Matrix
cm = confusion_matrix(y_pred,ytest.values)

# Accuracy
accuracy=accuracy_score(y_pred,ytest.values)

# AUC
auc = roc_auc_score(ytest,clf.predict(X_test))
```


```python
cm
```




    array([[1791, 1510],
           [ 732, 4008]])




```python
accuracy
```




    0.7211789578410646




```python
auc
```




    0.8106415540277006



Function that calculates for different thresholds:
 - Precision
 - Recall
 - F1 score


```python
def prec_rec(model, features, y, threshold):
    y_est_1=model.predict(features)
    for i in range(0,len(features)):
        if y_est_1[i]>=threshold:
            y_est_1[i]=1
        else:
            y_est_1[i]=0
    
    aux_1=(
        y.to_frame() >>
        mutate(y_pred = y_est_1)
    )
    aux_1.columns = ['y_obs', 'y_est']
    tp_1 = sum(np.where((aux_1['y_obs'] == 1)  & (aux_1['y_est'] == 1), 1, 0))
    fp_1 = sum(np.where((aux_1['y_obs'] == 0)  & (aux_1['y_est'] == 1), 1, 0))
    fn_1 = sum(np.where((aux_1['y_obs'] == 1)  & (aux_1['y_est'] == 0), 1, 0))
    tn_1 = sum(np.where((aux_1['y_obs'] == 0)  & (aux_1['y_est'] == 0), 1, 0))
    precision_1 = tp_1/(tp_1+fp_1)
    recall_1 = tp_1/(tp_1+fn_1)
    f1_score_1=1/(((1/recall_1)+(1/precision_1))/2)
    y_est_0=model.predict(features)
    for i in range(0,len(features)):
        if y_est_0[i]<threshold:
           y_est_0[i]=1
        else:
           y_est_0[i]=0

    aux_0=(
    y.to_frame() >>
        mutate(y_pred = y_est_1)
    )
    aux_0.columns = ['y_obs', 'y_est']
    tp_0 = sum(np.where((aux_0['y_obs'] == 0)  & (aux_0['y_est'] == 0), 1, 0))
    fp_0 = sum(np.where((aux_0['y_obs'] == 1)  & (aux_0['y_est'] == 0), 1, 0))
    fn_0 = sum(np.where((aux_0['y_obs'] == 0)  & (aux_0['y_est'] == 1), 1, 0))
    tn_0 = sum(np.where((aux_0['y_obs'] == 1)  & (aux_0['y_est'] == 1), 1, 0))
    precision_0 = tp_0/(tp_0+fp_0)
    recall_0 = tp_0/(tp_0+fn_0)
    f1_score_0=1/(((1/recall_0)+(1/precision_0))/2)
    return(f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0)
```


```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf, X_test, ytest, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.843262</td>
      <td>0.82702</td>
      <td>0.812245</td>
      <td>0.799238</td>
      <td>0.781439</td>
      <td>0.766663</td>
      <td>0.743511</td>
      <td>0.713305</td>
      <td>0.66712</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.610786</td>
      <td>0.615939</td>
      <td>0.618257</td>
      <td>0.62184</td>
      <td>0.615041</td>
      <td>0.615638</td>
      <td>0.60935</td>
      <td>0.604851</td>
      <td>0.594788</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.812847</td>
      <td>0.82316</td>
      <td>0.832003</td>
      <td>0.841789</td>
      <td>0.84557</td>
      <td>0.854533</td>
      <td>0.861337</td>
      <td>0.874277</td>
      <td>0.889459</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.673352</td>
      <td>0.62242</td>
      <td>0.58978</td>
      <td>0.56778</td>
      <td>0.542563</td>
      <td>0.526464</td>
      <td>0.504285</td>
      <td>0.482425</td>
      <td>0.456025</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.876042</td>
      <td>0.830917</td>
      <td>0.793403</td>
      <td>0.760783</td>
      <td>0.72635</td>
      <td>0.695179</td>
      <td>0.654041</td>
      <td>0.602392</td>
      <td>0.533708</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.558859</td>
      <td>0.609592</td>
      <td>0.649623</td>
      <td>0.687277</td>
      <td>0.709869</td>
      <td>0.741181</td>
      <td>0.769719</td>
      <td>0.810543</td>
      <td>0.854935</td>
    </tr>
  </tbody>
</table>
</div>



### Accuracy Validation


```python
y_pred_v=clf.predict(Xv)
for i in range(0,len(Xv)):
    if y_pred_v[i]>=.5:
       y_pred_v[i]=1
    else:  
       y_pred_v[i]=0
```


```python
cm_v = confusion_matrix(y_pred_v,yv.values)
accuracy_v=accuracy_score(y_pred_v,yv.values)
auc_v = roc_auc_score(yv,clf.predict(Xv))
```


```python
cm_v
```




    array([[2309, 1846],
           [ 937, 4959]])




```python
accuracy_v
```




    0.7231121281464531




```python
auc_v
```




    0.8082674069436278




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf, Xv, yv, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.839048</td>
      <td>0.824192</td>
      <td>0.809002</td>
      <td>0.796184</td>
      <td>0.780883</td>
      <td>0.761712</td>
      <td>0.741414</td>
      <td>0.706843</td>
      <td>0.657972</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.620448</td>
      <td>0.626011</td>
      <td>0.62404</td>
      <td>0.627164</td>
      <td>0.62397</td>
      <td>0.619656</td>
      <td>0.617321</td>
      <td>0.606802</td>
      <td>0.595832</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.810012</td>
      <td>0.820352</td>
      <td>0.82636</td>
      <td>0.835594</td>
      <td>0.841079</td>
      <td>0.847497</td>
      <td>0.856675</td>
      <td>0.863935</td>
      <td>0.877296</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.677737</td>
      <td>0.632307</td>
      <td>0.599263</td>
      <td>0.577352</td>
      <td>0.555716</td>
      <td>0.533467</td>
      <td>0.514815</td>
      <td>0.487828</td>
      <td>0.459953</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.870242</td>
      <td>0.828068</td>
      <td>0.792359</td>
      <td>0.760323</td>
      <td>0.728729</td>
      <td>0.691697</td>
      <td>0.65349</td>
      <td>0.59809</td>
      <td>0.526378</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.572089</td>
      <td>0.61984</td>
      <td>0.650955</td>
      <td>0.686383</td>
      <td>0.711337</td>
      <td>0.739063</td>
      <td>0.770795</td>
      <td>0.802526</td>
      <td>0.845656</td>
    </tr>
  </tbody>
</table>
</div>



## Feature importance


```python
lgb.plot_importance(clf,max_num_features=10, title='Feature Importance - Top 10')
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1c64e70b70>




![png](output_263_1.png)


## Partial Plots - Top 5 & pur

Create a matrix with the average of each feature of the validation frame


```python
avg_frame=Xv.mean(axis=0).to_frame().T
avg_frame
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>total_loans</th>
      <th>total_loan_redemption</th>
      <th>total_drop_loan</th>
      <th>total_Originalloan_loan_redemption</th>
      <th>total_Originalloan_drop_loan</th>
      <th>total_RenewalDollars_loan_redemption</th>
      <th>total_RenewalDollars_drop_loan</th>
      <th>total_items</th>
      <th>avg_items_per_loan</th>
      <th>sd_items_per_loan</th>
      <th>...</th>
      <th>Tamaulipas</th>
      <th>Tlaxcala</th>
      <th>Veracruz</th>
      <th>Yucatan</th>
      <th>Zacatecas</th>
      <th>center</th>
      <th>north</th>
      <th>south</th>
      <th>F</th>
      <th>M</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>6.165954</td>
      <td>3.555567</td>
      <td>2.610387</td>
      <td>4646.448679</td>
      <td>3174.39407</td>
      <td>838.077953</td>
      <td>528.68427</td>
      <td>6.693563</td>
      <td>1.077921</td>
      <td>0.116132</td>
      <td>...</td>
      <td>0.022684</td>
      <td>0.014028</td>
      <td>0.188041</td>
      <td>0.000398</td>
      <td>0.000298</td>
      <td>0.733658</td>
      <td>0.084867</td>
      <td>0.181474</td>
      <td>0.205154</td>
      <td>0.794846</td>
    </tr>
  </tbody>
</table>
<p>1 rows × 142 columns</p>
</div>



To create the Partial Plots, we are going to use 20 quantiles, so we need a frame with the average of each column 20 times


```python
avg_frame_20=pd.DataFrame(np.repeat(avg_frame.values,21,axis=0))
avg_frame_20.columns = avg_frame.columns
```

Function to create partial plot


```python
def partial_plot(model,frame,feature):
    df_aux=frame
    df_aux[feature]=Xv[feature].quantile(
        q=[0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55,
           0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99]
       ).values
    df_aux['mean_response']=model.predict(df_aux)
    pplot=(
        df_aux >>
        ggplot() +
        ggtitle(feature+' - Partial Plot') +
        theme_bw() +
        scale_y_continuous(labels = percent_format()) +
        geom_smooth(aes(x = feature, y = 'mean_response'), colour='darkblue')
    )
    return(pplot)
```

Top 10 features in a vector


```python
fi = pd.DataFrame(sorted(zip(clf.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])
top10=(
fi >>
    select('Feature') >>
    head(10) >>
    pull
)
```


```python
partial_plot(clf,avg_frame_20,top10[0])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_273_1.png)





    <ggplot: (7542387208)>




```python
partial_plot(clf,avg_frame_20,top10[1])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_274_1.png)





    <ggplot: (7543010459)>




```python
partial_plot(clf,avg_frame_20,top10[2])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_275_1.png)





    <ggplot: (-9223372029312443630)>




```python
partial_plot(clf,avg_frame_20,top10[3])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_276_1.png)





    <ggplot: (7601344895)>




```python
partial_plot(clf,avg_frame_20,top10[4])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_277_1.png)





    <ggplot: (7001336801)>




```python
partial_plot(clf,avg_frame_20,'pur')
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_278_1.png)





    <ggplot: (-9223372029202337939)>



## Effectivenes


```python
predict_ = pd.DataFrame(clf.predict(Xv), columns=['predict'])

predict_efect = pd.DataFrame([predict_.predict.values, yv.values]).T

predict_efect.columns = ['predict', 'y_red']

predict_efect['r_score'] = pd.qcut(predict_efect.predict, q=[0.01, 0.1,0.2,0.3,0.4,0.5,0.6,
                                                         0.7,0.8,0.9,1.0], 
                                   labels=['q10%','q20%','q30%','q40%','q50%','q60%','q70%','q80%','q90%','q100%'])

a = predict_efect.loc[predict_efect.y_red == 1].groupby(['r_score']).y_red.count() /predict_efect.groupby(['r_score']).y_red.count()

a.plot.bar()
plt.title('Backtest Efectiveness',fontsize=20)
plt.legend(['Efectiveness'])
plt.xticks(fontsize=10)
plt.plot([-1,100],[a.mean(),a.mean()],color='blue')
plt.text(0.2,0.7,'Mean Efectiveness')
for i in range(len(a)):
    plt.text(i-0.5, a[i], str((a[i]*100).round(2))+'%')
```


![png](output_280_0.png)


## Non used features


```python
nu = pd.DataFrame(sorted(zip(clf.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])

nu[nu['Value']==0]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Value</th>
      <th>Feature</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>116</th>
      <td>0</td>
      <td>total_extensions</td>
    </tr>
    <tr>
      <th>117</th>
      <td>0</td>
      <td>total_extensiondollars</td>
    </tr>
    <tr>
      <th>118</th>
      <td>0</td>
      <td>sd_extensions_per_loan</td>
    </tr>
    <tr>
      <th>119</th>
      <td>0</td>
      <td>sd_extensiondollars_per_loan</td>
    </tr>
    <tr>
      <th>120</th>
      <td>0</td>
      <td>pct_Watch</td>
    </tr>
    <tr>
      <th>121</th>
      <td>0</td>
      <td>pct_Other</td>
    </tr>
    <tr>
      <th>122</th>
      <td>0</td>
      <td>pct_Money_Clip</td>
    </tr>
    <tr>
      <th>123</th>
      <td>0</td>
      <td>pct_Long_Guns</td>
    </tr>
    <tr>
      <th>124</th>
      <td>0</td>
      <td>pct_Hand_Guns</td>
    </tr>
    <tr>
      <th>125</th>
      <td>0</td>
      <td>min_extensions_per_loan</td>
    </tr>
    <tr>
      <th>126</th>
      <td>0</td>
      <td>max_extensions_per_loan</td>
    </tr>
    <tr>
      <th>127</th>
      <td>0</td>
      <td>avg_pct_extensiondollars_originalloan_per_loan</td>
    </tr>
    <tr>
      <th>128</th>
      <td>0</td>
      <td>avg_extensions_per_loan</td>
    </tr>
    <tr>
      <th>129</th>
      <td>0</td>
      <td>avg_extensiondollars_per_loan</td>
    </tr>
    <tr>
      <th>130</th>
      <td>0</td>
      <td>Zacatecas</td>
    </tr>
    <tr>
      <th>131</th>
      <td>0</td>
      <td>Watch</td>
    </tr>
    <tr>
      <th>132</th>
      <td>0</td>
      <td>Sonora</td>
    </tr>
    <tr>
      <th>133</th>
      <td>0</td>
      <td>Other</td>
    </tr>
    <tr>
      <th>134</th>
      <td>0</td>
      <td>Nayarit</td>
    </tr>
    <tr>
      <th>135</th>
      <td>0</td>
      <td>Money_Clip</td>
    </tr>
    <tr>
      <th>136</th>
      <td>0</td>
      <td>Long_Guns</td>
    </tr>
    <tr>
      <th>137</th>
      <td>0</td>
      <td>Hand_Guns</td>
    </tr>
    <tr>
      <th>138</th>
      <td>0</td>
      <td>Durango</td>
    </tr>
    <tr>
      <th>139</th>
      <td>0</td>
      <td>Colima</td>
    </tr>
    <tr>
      <th>140</th>
      <td>0</td>
      <td>Chihuahua</td>
    </tr>
    <tr>
      <th>141</th>
      <td>0</td>
      <td>Baja California Sur</td>
    </tr>
  </tbody>
</table>
</div>



## Save Model


```python
output=open('lgbm.pkl', 'wb')
pickle.dump(clf, output)
output.close()
```

# Redeem Model - Optimice Hiperparameters

Grid


```python
p_learning_rate = np.array([0.1, 0.2])
p_max_depth = np.array([20,30])
p_num_leaves = np.array([100,200,300])
p_num_iterations = np.array([100,200,300])
```

AUC in test Matrix


```python
hp_perf_mat = np.zeros((p_learning_rate.shape[0],
                        p_max_depth.shape[0],
                        p_num_leaves.shape[0],
                        p_num_iterations.shape[0]))
i=1
```


```python
%%time
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                params = {}
                params['learning_rate'] = p_learning_rate[i]
                params['boosting_type'] = 'gbdt'
                params['objective'] = 'binary'
                params['metric'] = 'binary_logloss'
                params['sub_feature'] = 0.5
                params['num_leaves'] = p_num_leaves[k]
                params['min_data'] = 10
                params['max_depth'] = p_max_depth[j]
                params['num_iterations'] = p_num_iterations[l]
                lgbm = lgb.train(params, d_train, 10)
                # Print each iteration
                print(str(i)+str(j)+str(k)+str(l))
                # Evaluate AUC in TEST
                auc_i = roc_auc_score(ytest,lgbm.predict(X_test))
                # Add the AUC to the AUC Matrix
                hp_perf_mat[i,j,k,l] = auc_i
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0000


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0001


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0002


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0010


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0011


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0012


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0020


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0021


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0022


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0100


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0101


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0102


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0110


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0111


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0112


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0120


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0121


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0122


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1000


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1001


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1002


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1010


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1011


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1012


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1020


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1021


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1022


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1100


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1101


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1102


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1110


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1111


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1112


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1120


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1121


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1122
    CPU times: user 9min 36s, sys: 13min 4s, total: 22min 40s
    Wall time: 8min 27s


## Best Hiperparameters

Create a DataFrame with all the posible combinations of hiperparameters


```python
# Number of iterations
it=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                it=it+1
```

Create a schema dataframe with zeros


```python
auc_data = np.zeros(shape=(it,5))
auc_data=pd.DataFrame(auc_data,columns=['learn_rate',
                                        'max_depth',
                                        'num_leaves',
                                        'num_iterations',
                                        'AUC'])
```

Fill the DataFrame with the combinations of hiperparameters


```python
it_row=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                auc_data['learn_rate'][it_row]=p_learning_rate[i]
                auc_data['max_depth'][it_row]=p_max_depth[j]
                auc_data['num_leaves'][it_row]=p_num_leaves[j]
                auc_data['num_iterations'][it_row]=p_num_iterations[l]
                it_row=it_row+1
```

Fill the data frame with the AUC for each combination of hiperparameters


```python
it_row=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                auc_data['AUC'][it_row]=hp_perf_mat[i,j,k,l]
                it_row=it_row+1
```

### Plot the AUC


```python
auc_data['Grid']=auc_data['learn_rate'].astype('str')+'-'+auc_data['max_depth'].astype('str')+'-'+auc_data['num_leaves'].astype('str')+'-'+auc_data['num_iterations'].astype('str')
```


```python
(
auc_data >>
    ggplot() +
    geom_line(aes(x = 'Grid', y = 'AUC', group = 1), colour='darkblue') +
    geom_point(aes(x = 'Grid', y = 'AUC'), colour='darkblue', size = 0.2) +
    ggtitle('AUC in Test - Grid \n Learn_rate,Max_depth,Num_leaves,Num_iterations') +
    theme_bw() +
    theme(axis_text_x = element_text(angle = 90))
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:517: MatplotlibDeprecationWarning: isinstance(..., numbers.Number)
      return not cbook.iterable(value) and (cbook.is_numlike(value) or



![png](output_302_1.png)





    <ggplot: (7542406520)>



### Optimial Hiperparamters


```python
auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>learn_rate</th>
      <th>max_depth</th>
      <th>num_leaves</th>
      <th>num_iterations</th>
      <th>AUC</th>
      <th>Grid</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>17</th>
      <td>0.1</td>
      <td>30.0</td>
      <td>200.0</td>
      <td>300.0</td>
      <td>0.811624</td>
      <td>0.1-30.0-200.0-300.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
opt_learn_rate=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['learn_rate']
opt_max_depth=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['max_depth']
opt_num_leaves=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_leaves']
opt_num_iterations=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_iterations']
```

## Run Model with Optimal Hiperparameters


```python
params_opt = {}
params_opt['learning_rate'] = float(opt_learn_rate)
params_opt['boosting_type'] = 'gbdt'
params_opt['objective'] = 'binary'
params_opt['metric'] = 'binary_logloss'
params_opt['sub_feature'] = 0.5
params_opt['num_leaves'] = int(float(opt_num_leaves))
params_opt['min_data'] = 10
params_opt['max_depth'] = int(float(opt_max_depth))
params_opt['num_iterations'] = int(float(opt_num_iterations))
clf_opt = lgb.train(params_opt, d_train, 10) 
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


## Accuracy

### Accuray in test


```python
y_pred_opt_t=clf_opt.predict(X_test)
for i in range(0,len(X_test)):
    if y_pred_opt_t[i]>=.5:
       y_pred_opt_t[i]=1
    else:  
       y_pred_opt_t[i]=0
```


```python
# Confussion Matrix
cm_opt_t = confusion_matrix(y_pred_opt_t,ytest.values)

# Accuracy
accuracy_opt_T = accuracy_score(y_pred_opt_t,ytest.values)

# AUC
auc_opt_t = roc_auc_score(ytest,clf_opt.predict(X_test))
```


```python
cm_opt_t
```




    array([[1795, 1490],
           [ 728, 4028]])




```python
accuracy_opt_T
```




    0.7241636612361646




```python
auc_opt_t
```




    0.8069512568458618




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf_opt, X_test, ytest, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.854966</td>
      <td>0.850204</td>
      <td>0.834692</td>
      <td>0.813578</td>
      <td>0.784115</td>
      <td>0.743841</td>
      <td>0.690495</td>
      <td>0.616511</td>
      <td>0.461896</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.522551</td>
      <td>0.59549</td>
      <td>0.610763</td>
      <td>0.620755</td>
      <td>0.618113</td>
      <td>0.610563</td>
      <td>0.602981</td>
      <td>0.587506</td>
      <td>0.551693</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.773507</td>
      <td>0.802348</td>
      <td>0.816464</td>
      <td>0.833207</td>
      <td>0.84693</td>
      <td>0.862539</td>
      <td>0.886616</td>
      <td>0.912287</td>
      <td>0.94249</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.799837</td>
      <td>0.709819</td>
      <td>0.64465</td>
      <td>0.592366</td>
      <td>0.546423</td>
      <td>0.504925</td>
      <td>0.469704</td>
      <td>0.435598</td>
      <td>0.3872</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.9556</td>
      <td>0.904132</td>
      <td>0.853751</td>
      <td>0.794853</td>
      <td>0.729975</td>
      <td>0.65386</td>
      <td>0.565422</td>
      <td>0.465567</td>
      <td>0.305908</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.38803</td>
      <td>0.512881</td>
      <td>0.580262</td>
      <td>0.652002</td>
      <td>0.711455</td>
      <td>0.772097</td>
      <td>0.841855</td>
      <td>0.902101</td>
      <td>0.959176</td>
    </tr>
  </tbody>
</table>
</div>



### Accuracy in validation


```python
y_pred_opt_v=clf_opt.predict(Xv)
for i in range(0,len(Xv)):
    if y_pred_opt_v[i]>=.5:
       y_pred_opt_v[i]=1
    else:  
       y_pred_opt_v[i]=0
```


```python
# Confussion Matrix
cm_opt_v = confusion_matrix(y_pred_opt_v,yv.values)

# Accuracy
accuracy_opt_v=accuracy_score(y_pred_opt_v,yv.values)

# AUC
auc_opt_v = roc_auc_score(yv,clf_opt.predict(Xv))
```


```python
cm_opt_v
```




    array([[2322, 1851],
           [ 924, 4954]])




```python
accuracy_opt_v
```




    0.7239080688488707




```python
auc_opt_v
```




    0.8079531332973879




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf_opt, Xv, yv, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.852524</td>
      <td>0.848786</td>
      <td>0.832053</td>
      <td>0.809677</td>
      <td>0.781203</td>
      <td>0.741249</td>
      <td>0.689809</td>
      <td>0.610457</td>
      <td>0.466865</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.540729</td>
      <td>0.60899</td>
      <td>0.621215</td>
      <td>0.623611</td>
      <td>0.62596</td>
      <td>0.621324</td>
      <td>0.611541</td>
      <td>0.593022</td>
      <td>0.561769</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.771133</td>
      <td>0.799896</td>
      <td>0.813536</td>
      <td>0.825695</td>
      <td>0.842804</td>
      <td>0.861592</td>
      <td>0.881747</td>
      <td>0.904501</td>
      <td>0.935071</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.805488</td>
      <td>0.723305</td>
      <td>0.654831</td>
      <td>0.600571</td>
      <td>0.556434</td>
      <td>0.515873</td>
      <td>0.480542</td>
      <td>0.442673</td>
      <td>0.397971</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.953123</td>
      <td>0.904041</td>
      <td>0.851433</td>
      <td>0.794269</td>
      <td>0.727994</td>
      <td>0.650404</td>
      <td>0.566495</td>
      <td>0.460691</td>
      <td>0.311095</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.406962</td>
      <td>0.525878</td>
      <td>0.590881</td>
      <td>0.64849</td>
      <td>0.715342</td>
      <td>0.780961</td>
      <td>0.840727</td>
      <td>0.898028</td>
      <td>0.954713</td>
    </tr>
  </tbody>
</table>
</div>



## Feature Importance


```python
lgb.plot_importance(clf_opt,max_num_features=10, title='Feature Importance - Top 10')
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1c19469cf8>




![png](output_326_1.png)


## Partial Plots - Top 5 & PUR


```python
avg_frame_opt=Xv.mean(axis=0).to_frame().T
avg_frame_opt_20=pd.DataFrame(np.repeat(avg_frame_opt.values,21,axis=0))
avg_frame_opt_20.columns = avg_frame_opt.columns
```

Top 10 features in a vector


```python
fi_opt = pd.DataFrame(sorted(zip(clf_opt.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])
top10_opt=(
fi_opt >>
    select('Feature') >>
    head(10) >>
    pull
)
```


```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[0])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_331_1.png)





    <ggplot: (7599915314)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[1])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_332_1.png)





    <ggplot: (7610526528)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[2])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_333_1.png)





    <ggplot: (-9223372029247554602)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[3])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_334_1.png)





    <ggplot: (-9223372029274225429)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[4])
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_335_1.png)





    <ggplot: (7598344701)>




```python
partial_plot(clf_opt,avg_frame_opt_20,'pur')
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/smoothers.py:146: UserWarning: Confidence intervals are not yet implementedfor lowess smoothings.
      warnings.warn("Confidence intervals are not yet implemented"



![png](output_336_1.png)





    <ggplot: (7613131563)>



## Efectiveness


```python
predict_ = pd.DataFrame(clf_opt.predict(Xv), columns=['predict'])

predict_efect = pd.DataFrame([predict_.predict.values, yv.values]).T

predict_efect.columns = ['predict', 'y_red']

predict_efect['r_score'] = pd.qcut(predict_efect.predict, q=[0.01, 0.1,0.2,0.3,0.4,0.5,0.6,
                                                         0.7,0.8,0.9,1.0], 
                                   labels=['q10%','q20%','q30%','q40%','q50%','q60%','q70%','q80%','q90%','q100%'])

a = predict_efect.loc[predict_efect.y_red == 1].groupby(['r_score']).y_red.count() /predict_efect.groupby(['r_score']).y_red.count()

a.plot.bar()
plt.title('Backtest Efectiveness',fontsize=20)
plt.legend(['Efectiveness'])
plt.xticks(fontsize=10)
plt.plot([-1,100],[a.mean(),a.mean()],color='blue')
plt.text(0.2,0.7,'Mean Efectiveness')
for i in range(len(a)):
    plt.text(i-0.5, a[i], str((a[i]*100).round(2))+'%')
```


![png](output_338_0.png)


## Non used features


```python
nu = pd.DataFrame(sorted(zip(clf_opt.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])

nu[nu['Value']==0]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Value</th>
      <th>Feature</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>116</th>
      <td>0</td>
      <td>total_extensions</td>
    </tr>
    <tr>
      <th>117</th>
      <td>0</td>
      <td>total_extensiondollars</td>
    </tr>
    <tr>
      <th>118</th>
      <td>0</td>
      <td>sd_extensions_per_loan</td>
    </tr>
    <tr>
      <th>119</th>
      <td>0</td>
      <td>sd_extensiondollars_per_loan</td>
    </tr>
    <tr>
      <th>120</th>
      <td>0</td>
      <td>pct_Watch</td>
    </tr>
    <tr>
      <th>121</th>
      <td>0</td>
      <td>pct_Other</td>
    </tr>
    <tr>
      <th>122</th>
      <td>0</td>
      <td>pct_Money_Clip</td>
    </tr>
    <tr>
      <th>123</th>
      <td>0</td>
      <td>pct_Long_Guns</td>
    </tr>
    <tr>
      <th>124</th>
      <td>0</td>
      <td>pct_Hand_Guns</td>
    </tr>
    <tr>
      <th>125</th>
      <td>0</td>
      <td>min_extensions_per_loan</td>
    </tr>
    <tr>
      <th>126</th>
      <td>0</td>
      <td>max_extensions_per_loan</td>
    </tr>
    <tr>
      <th>127</th>
      <td>0</td>
      <td>avg_pct_extensiondollars_originalloan_per_loan</td>
    </tr>
    <tr>
      <th>128</th>
      <td>0</td>
      <td>avg_extensions_per_loan</td>
    </tr>
    <tr>
      <th>129</th>
      <td>0</td>
      <td>avg_extensiondollars_per_loan</td>
    </tr>
    <tr>
      <th>130</th>
      <td>0</td>
      <td>Zacatecas</td>
    </tr>
    <tr>
      <th>131</th>
      <td>0</td>
      <td>Watch</td>
    </tr>
    <tr>
      <th>132</th>
      <td>0</td>
      <td>Sonora</td>
    </tr>
    <tr>
      <th>133</th>
      <td>0</td>
      <td>Other</td>
    </tr>
    <tr>
      <th>134</th>
      <td>0</td>
      <td>Nayarit</td>
    </tr>
    <tr>
      <th>135</th>
      <td>0</td>
      <td>Money_Clip</td>
    </tr>
    <tr>
      <th>136</th>
      <td>0</td>
      <td>Long_Guns</td>
    </tr>
    <tr>
      <th>137</th>
      <td>0</td>
      <td>Hand_Guns</td>
    </tr>
    <tr>
      <th>138</th>
      <td>0</td>
      <td>Durango</td>
    </tr>
    <tr>
      <th>139</th>
      <td>0</td>
      <td>Colima</td>
    </tr>
    <tr>
      <th>140</th>
      <td>0</td>
      <td>Chihuahua</td>
    </tr>
    <tr>
      <th>141</th>
      <td>0</td>
      <td>Baja California Sur</td>
    </tr>
  </tbody>
</table>
</div>



# Redeem Model - Top Features Selection

Trying to avoid the overfitting because of the use of a lot of features, a model is going to fit, using the unique top 10 features of the first model made, and the model created with the optimized hiperparameters.

## Feature Selection


```python
x_selection=list(set(top10.tolist()+top10_opt.tolist()))
x_selection
```




    ['total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'max_days_for_loan',
     'dif_max_avg_days_for_loan',
     'total_Originalloan_loan_redemption',
     'avg_days_for_loan',
     'age',
     'total_Originalloan_drop_loan',
     'sd_days_for_loan']



## LightGBM Frame


```python
d_train_selection = lgb.Dataset(X_data_downsampled[x_selection], label=y_data_downsampled)
```

## Grid Hiperparameters


```python
p_learning_rate_s = np.array([0.5,0.1,0.2])
p_max_depth_s = np.array([20,30])
p_num_leaves_s = np.array([100,200,300])
p_num_iterations_s = np.array([100,200,300])
```

AUC in test Matrix


```python
hp_perf_mat_s = np.zeros((p_learning_rate_s.shape[0],
                        p_max_depth_s.shape[0],
                        p_num_leaves_s.shape[0],
                        p_num_iterations_s.shape[0]))
i=1
```

## Fit Grid


```python
%%time
for i in range(p_learning_rate_s.shape[0]):
    for j in range(p_max_depth_s.shape[0]):
        for k in range(p_num_leaves_s.shape[0]):
            for l in range(p_num_iterations_s.shape[0]):
                params = {}
                params['learning_rate'] = p_learning_rate_s[i]
                params['boosting_type'] = 'gbdt'
                params['objective'] = 'binary'
                params['metric'] = 'binary_logloss'
                params['num_leaves'] = p_num_leaves_s[k]
                params['min_data'] = 10
                params['max_depth'] = p_max_depth_s[j]
                params['num_iterations'] = p_num_iterations_s[l]
                lgbm = lgb.train(params, d_train_selection, 10)
                # Print each iteration
                print(str(i)+str(j)+str(k)+str(l))
                # Evaluate AUC in TEST
                auc_i = roc_auc_score(ytest,lgbm.predict(X_test))
                # Add the AUC to the AUC Matrix
                hp_perf_mat_s[i,j,k,l] = auc_i
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0000


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0001


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0002


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0010


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0011


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0012


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0020


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0021


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0022


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0100


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0101


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0102


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0110


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0111


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0112


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0120


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0121


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0122


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1000


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1001


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1002


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1010


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1011


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1012


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1020


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1021


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1022


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1100


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1101


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1102


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1110


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1111


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1112


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1120


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1121


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    1122


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2000


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2001


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2002


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2010


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2011


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2012


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2020


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2021


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2022


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2100


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2101


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2102


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2110


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2111


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2112


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2120


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2121


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    2122
    CPU times: user 11min 25s, sys: 17min 24s, total: 28min 50s
    Wall time: 10min 59s


## Best Hiperparameters

Create a DataFrame with all the posible combinations of hiperparameters


```python
# Number of iterations
it=0
for i in range(p_learning_rate_s.shape[0]):
    for j in range(p_max_depth_s.shape[0]):
        for k in range(p_num_leaves_s.shape[0]):
            for l in range(p_num_iterations_s.shape[0]):
                it=it+1
```

Create a schema dataframe with zeros


```python
auc_data = np.zeros(shape=(it,5))
auc_data=pd.DataFrame(auc_data,columns=['learn_rate',
                                        'max_depth',
                                        'num_leaves',
                                        'num_iterations',
                                        'AUC'])
```

Fill the DataFrame with the combinations of hiperparameters


```python
it_row=0
for i in range(p_learning_rate_s.shape[0]):
    for j in range(p_max_depth_s.shape[0]):
        for k in range(p_num_leaves_s.shape[0]):
            for l in range(p_num_iterations_s.shape[0]):
                auc_data['learn_rate'][it_row]=p_learning_rate_s[i]
                auc_data['max_depth'][it_row]=p_max_depth_s[j]
                auc_data['num_leaves'][it_row]=p_num_leaves_s[j]
                auc_data['num_iterations'][it_row]=p_num_iterations_s[l]
                it_row=it_row+1
```

Fill the data frame with the AUC for each combination of hiperparameters


```python
it_row=0
for i in range(p_learning_rate_s.shape[0]):
    for j in range(p_max_depth_s.shape[0]):
        for k in range(p_num_leaves_s.shape[0]):
            for l in range(p_num_iterations_s.shape[0]):
                auc_data['AUC'][it_row]=hp_perf_mat_s[i,j,k,l]
                it_row=it_row+1
```

### Plot AUC


```python
auc_data['Grid']=auc_data['learn_rate'].astype('str')+'-'+auc_data['max_depth'].astype('str')+'-'+auc_data['num_leaves'].astype('str')+'-'+auc_data['num_iterations'].astype('str')
```


```python
(
auc_data >>
    ggplot() +
    geom_line(aes(x = 'Grid', y = 'AUC', group = 1), colour='darkblue') +
    geom_point(aes(x = 'Grid', y = 'AUC'), colour='darkblue', size = 0.2) +
    ggtitle('AUC in Test Feature Selection - Grid \n Learn_rate,Max_depth,Num_leaves,Num_iterations') +
    theme_bw() +
    theme(axis_text_x = element_text(angle = 90))
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:517: MatplotlibDeprecationWarning: isinstance(..., numbers.Number)
      return not cbook.iterable(value) and (cbook.is_numlike(value) or



![png](output_364_1.png)





    <ggplot: (-9223372029232758405)>



### Optimal Hiperparameters


```python
auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>learn_rate</th>
      <th>max_depth</th>
      <th>num_leaves</th>
      <th>num_iterations</th>
      <th>AUC</th>
      <th>Grid</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>42</th>
      <td>0.2</td>
      <td>20.0</td>
      <td>100.0</td>
      <td>100.0</td>
      <td>0.684679</td>
      <td>0.2-20.0-100.0-100.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
opt_learn_rate_s=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['learn_rate']
opt_max_depth_s=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['max_depth']
opt_num_leaves_s=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_leaves']
opt_num_iterations_s=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_iterations']
```

## Run Model with Optimal Hiperparameters


```python
params_opt_s = {}
params_opt_s['learning_rate'] = float(opt_learn_rate_s)
params_opt_s['boosting_type'] = 'gbdt'
params_opt_s['objective'] = 'binary'
params_opt_s['metric'] = 'binary_logloss'
params_opt_s['num_leaves'] = int(float(opt_num_leaves_s))
params_opt_s['min_data'] = 10
params_opt_s['max_depth'] = int(float(opt_max_depth))
params_opt_s['num_iterations'] = int(float(opt_num_iterations_s))
clf_opt_s = lgb.train(params_opt_s, d_train_selection, 10) 
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


## Accuracy

### Accuray in test


```python
y_pred_opt_t_s=clf_opt_s.predict(X_test)
for i in range(0,len(X_test)):
    if y_pred_opt_t_s[i]>=.5:
       y_pred_opt_t_s[i]=1
    else:  
       y_pred_opt_t_s[i]=0
```


```python
# Confussion Matrix
cm_opt_t_s = confusion_matrix(y_pred_opt_t_s,ytest.values)

# Accuracy
accuracy_opt_T_s = accuracy_score(y_pred_opt_t_s,ytest.values)

# AUC
auc_opt_t_s = roc_auc_score(ytest,clf_opt_s.predict(X_test))
```


```python
cm_opt_t_s
```




    array([[1652, 2385],
           [ 871, 3133]])




```python
accuracy_opt_T_s
```




    0.5950752393980848




```python
auc_opt_t_s
```




    0.6031237874332509




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf_opt_s, X_test, ytest, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.812921</td>
      <td>0.714413</td>
      <td>0.703026</td>
      <td>0.660567</td>
      <td>0.658055</td>
      <td>0.654338</td>
      <td>0.606006</td>
      <td>0.543885</td>
      <td>0.455263</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.0232108</td>
      <td>0.345209</td>
      <td>0.350119</td>
      <td>0.399725</td>
      <td>0.503659</td>
      <td>0.505817</td>
      <td>0.504702</td>
      <td>0.508654</td>
      <td>0.511908</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.687555</td>
      <td>0.704225</td>
      <td>0.702899</td>
      <td>0.713565</td>
      <td>0.782468</td>
      <td>0.784791</td>
      <td>0.789183</td>
      <td>0.803685</td>
      <td>0.830932</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.483871</td>
      <td>0.357052</td>
      <td>0.350258</td>
      <td>0.353317</td>
      <td>0.409215</td>
      <td>0.408691</td>
      <td>0.3907</td>
      <td>0.377275</td>
      <td>0.364323</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.994201</td>
      <td>0.7249</td>
      <td>0.703153</td>
      <td>0.614897</td>
      <td>0.567778</td>
      <td>0.561073</td>
      <td>0.491845</td>
      <td>0.411018</td>
      <td>0.313519</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.0118906</td>
      <td>0.334126</td>
      <td>0.34998</td>
      <td>0.460166</td>
      <td>0.654776</td>
      <td>0.663496</td>
      <td>0.712644</td>
      <td>0.78042</td>
      <td>0.860484</td>
    </tr>
  </tbody>
</table>
</div>



### Accuracy in validation


```python
y_pred_opt_v_s=clf_opt_s.predict(Xv)
for i in range(0,len(Xv)):
    if y_pred_opt_v_s[i]>=.5:
       y_pred_opt_v_s[i]=1
    else:  
       y_pred_opt_v_s[i]=0
```


```python
# Confussion Matrix
cm_opt_v_s = confusion_matrix(y_pred_opt_v_s,yv.values)

# Accuracy
accuracy_opt_v_s=accuracy_score(y_pred_opt_v_s,yv.values)

# AUC
auc_opt_v_s = roc_auc_score(yv,clf_opt_s.predict(Xv))
```


```python
cm_opt_v_s
```




    array([[2060, 2946],
           [1186, 3859]])




```python
accuracy_opt_v_s
```




    0.5888966272012736




```python
auc_opt_v_s
```




    0.5963516732061119




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf_opt_s, Xv, yv, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.805839</td>
      <td>0.712225</td>
      <td>0.703274</td>
      <td>0.655753</td>
      <td>0.651308</td>
      <td>0.648172</td>
      <td>0.605282</td>
      <td>0.537418</td>
      <td>0.446625</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.0174857</td>
      <td>0.339128</td>
      <td>0.349635</td>
      <td>0.400818</td>
      <td>0.499273</td>
      <td>0.501263</td>
      <td>0.510145</td>
      <td>0.512365</td>
      <td>0.516177</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.677655</td>
      <td>0.692735</td>
      <td>0.693528</td>
      <td>0.702113</td>
      <td>0.764916</td>
      <td>0.76676</td>
      <td>0.778599</td>
      <td>0.789519</td>
      <td>0.814152</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>0.408451</td>
      <td>0.362553</td>
      <td>0.360747</td>
      <td>0.359501</td>
      <td>0.411506</td>
      <td>0.411126</td>
      <td>0.39972</td>
      <td>0.383333</td>
      <td>0.370103</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>0.993828</td>
      <td>0.732843</td>
      <td>0.713299</td>
      <td>0.615136</td>
      <td>0.567083</td>
      <td>0.561352</td>
      <td>0.495077</td>
      <td>0.407348</td>
      <td>0.307715</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.00893407</td>
      <td>0.318546</td>
      <td>0.339187</td>
      <td>0.452865</td>
      <td>0.634627</td>
      <td>0.642021</td>
      <td>0.704868</td>
      <td>0.772335</td>
      <td>0.852742</td>
    </tr>
  </tbody>
</table>
</div>



## Feature Importance


```python
lgb.plot_importance(clf_opt_s,max_num_features=10, title='Feature Importance - Top 10')
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1c59e08780>




![png](output_388_1.png)


## Efectiveness


```python
predict_s = pd.DataFrame(clf_opt_s.predict(Xv), columns=['predict'])

predict_efect_s = pd.DataFrame([predict_s.predict.values, yv.values]).T

predict_efect_s.columns = ['predict', 'y_red']

predict_efect_s['r_score'] = pd.qcut(predict_efect_s.predict, q=[0.01, 0.1,0.2,0.3,0.4,0.5,0.6,
                                                         0.7,0.8,0.9,1.0], 
                                   labels=['q10%','q20%','q30%','q40%','q50%','q60%','q70%','q80%','q90%','q100%'])

a = predict_efect_s.loc[predict_efect_s.y_red == 1].groupby(['r_score']).y_red.count() /predict_efect_s.groupby(['r_score']).y_red.count()

a.plot.bar()
plt.title('Backtest Efectiveness',fontsize=20)
plt.legend(['Efectiveness'])
plt.xticks(fontsize=10)
plt.plot([-1,100],[a.mean(),a.mean()],color='blue')
plt.text(0.2,0.7,'Mean Efectiveness')
for i in range(len(a)):
    plt.text(i-0.5, a[i], str((a[i]*100).round(2))+'%')
```


![png](output_390_0.png)



```python
(datas.columns).tolist()
```




    ['Customer',
     'total_loans',
     'total_loan_redemption',
     'total_drop_loan',
     'total_Originalloan_loan_redemption',
     'total_Originalloan_drop_loan',
     'total_RenewalDollars_loan_redemption',
     'total_RenewalDollars_drop_loan',
     'total_items',
     'avg_items_per_loan',
     'sd_items_per_loan',
     'max_items_per_loan',
     'total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'total_extensions',
     'avg_extensions_per_loan',
     'sd_extensions_per_loan',
     'min_extensions_per_loan',
     'max_extensions_per_loan',
     'total_extensiondollars',
     'avg_extensiondollars_per_loan',
     'sd_extensiondollars_per_loan',
     'total_renewals',
     'avg_renewals',
     'max_renewals',
     'min_renewals',
     'total_RenewalDollars',
     'avg_RenewalDollars',
     'sd_RenewalDollars',
     'avg_pct_extensiondollars_originalloan_per_loan',
     'avg_pct_RenewalDollars_originalloan_per_loan',
     'Gender',
     'City',
     'state',
     'birth_date',
     'zip',
     'avg_days_for_loan',
     'sd_days_for_loan',
     'max_days_for_loan',
     'min_days_for_loan',
     'dif_max_avg_days_for_loan',
     'dif_avg_min_days_for_loan',
     'Tools',
     'Electronics',
     'Household_Office_Goods',
     'Musical_Instruments',
     'Sporting_Goods',
     'Transportation',
     'Bracelet',
     'Ring',
     'Chain',
     'Yard_Equipment',
     'Camera_Equipment',
     'Pendant_Locket',
     'Gun_Police_Equipment',
     'Earrings',
     'Hand_Guns',
     'Long_Guns',
     'Money_Clip',
     'Watch',
     'Other',
     'lastAction',
     'y_red',
     'pct_loans_residence_state',
     'pct_store_Aguascalientes',
     'pct_store_Campeche',
     'pct_store_Chiapas',
     'pct_store_Coahuila',
     'pct_store_DistritoFederal',
     'pct_store_Guanajuato',
     'pct_store_Guerrero',
     'pct_store_Hidalgo',
     'pct_store_Jalisco',
     'pct_store_Mexico',
     'pct_store_MichoacanDeOcampo',
     'pct_store_Morelos',
     'pct_store_NuevoLeon',
     'pct_store_Oaxaca',
     'pct_store_Puebla',
     'pct_store_Queretaro',
     'pct_store_QuintanaRoo',
     'pct_store_SanLuisPotosi',
     'pct_store_Sinaloa',
     'pct_store_Tabasco',
     'pct_store_Tamaulipas',
     'pct_store_Tlaxcala',
     'pct_store_Veracruz',
     'age',
     'pct_drop_loan',
     'pct_loan_redemption',
     'pct_Tools',
     'pct_Electronics',
     'pct_Household_Office_Goods',
     'pct_Musical_Instruments',
     'pct_Sporting_Goods',
     'pct_Transportation',
     'pct_Bracelet',
     'pct_Ring',
     'pct_Chain',
     'pct_Yard_Equipment',
     'pct_Camera_Equipment',
     'pct_Pendant_Locket',
     'pct_Gun_Police_Equipment',
     'pct_Earrings',
     'pct_Hand_Guns',
     'pct_Long_Guns',
     'pct_Money_Clip',
     'pct_Watch',
     'pct_Other',
     'pct_money_drop',
     'pct_money_redeem',
     'pur',
     'ind_geo',
     'per',
     'Aguascalientes',
     'Baja California',
     'Baja California Sur',
     'Campeche',
     'Chiapas',
     'Chihuahua',
     'Coahuila',
     'Colima',
     'Distrito Federal',
     'Durango',
     'Guanajuato',
     'Guerrero',
     'Hidalgo',
     'Jalisco',
     'Mexico',
     'Michoacan De Ocampo',
     'Morelos',
     'Nayarit',
     'Nuevo Leon',
     'Oaxaca',
     'Puebla',
     'Queretaro',
     'Quintana Roo',
     'San Luis Potosi',
     'Sinaloa',
     'Sonora',
     'Tabasco',
     'Tamaulipas',
     'Tlaxcala',
     'Veracruz',
     'Yucatan',
     'Zacatecas',
     'center',
     'north',
     'south',
     'F',
     'M']


